import etc.linux.memoryerror;
import ongra;
import std.conv;
import std.file;
import std.getopt;
import std.path;
import vibe.core.core;
import vibe.http.server;

void main(string[] args) {
	static if (is(typeof(registerMemoryErrorHandler)))
		registerMemoryErrorHandler();
	
	string configFilePath = null;
	getopt(args,
		"config", &configFilePath,
	);
	
	Config.load(configFilePath ? configFilePath : "ongra.ini");
	Language.load(Config.server.LanguagesDir ~ "/en.xml");
	Db.load();
	
	// Create file for logging errors
	mkdirRecurse(dirName(Config.server.ErrorLog));
	append(Config.server.ErrorLog, null);
	
	auto http = new HTTPServerSettings;
	http.bindAddresses = [Config.server.Host];
	http.port = Config.server.Port.to!ushort;
	
	listenHTTP(http, App.router);
	lowerPrivileges();
	runEventLoop();
}