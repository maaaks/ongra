jQuery(document).ready(function($)
{
	$(document).delegate("div.cut", "click", function() {
		var cut = $(this);
		var undercut = cut.next("div.undercut");
		
		cut.slideUp(500);
		undercut.slideDown(500);
		
		return false;
	});
	
	$(document).delegate("span.cut", "click", function() {
		var cutid = $(this).data("cutid");
		
		$(this).fadeOut(200, function() {
			$("span.undercut.cut-"+cutid).fadeIn(500);
			$("div.undercut.cut-"+cutid).slideDown(500);
			
			// Adjust height
			var part2 = $(this).parents("part2");
			if (part2.length > 0) {
				part2.css("maxHeight", part2.prop("height"));
				part2.stop().animate({maxHeight: part2.prop("scrollHeight")}, 300, "linear");
			}
		});
	});
});