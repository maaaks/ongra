document.addEventListener("DOMContentLoaded", function()
{
	/*******************************************************
	
		Audio players
	
	*******************************************************/
	
	var allAudioPlayers = document.querySelectorAll(".player.audio");
	for (var i=0; i<allAudioPlayers.length; i++) {
		(function() {
			var player = allAudioPlayers[i];
			
			var audio = player.getElementsByTagName("audio")[0];
			var play = player.getElementsByClassName("play")[0];
			var download = player.getElementsByClassName("download")[0];
			var progress = player.getElementsByTagName("progress")[0];
			
			play.onclick = function() {
				if (play.classList.contains("pause")) {
					audio.pause();
					play.classList.remove("pause");
					play.classList.add("play");
				}
				else {
					audio.play();
					play.classList.remove("play");
					play.classList.remove("replay");
					play.classList.add("pause");
				}
			};
			
			// Controlling playback and changing icons by the Play/Pause/Replay button
			audio.onplay = function() {
				play.classList.remove("play");
				play.classList.remove("replay");
				play.classList.add("pause");
			};
			audio.onpause = function() {
				play.classList.add("play");
				play.classList.remove("replay");
				play.classList.remove("pause");
			};
			audio.onended = function() {
				play.classList.remove("play");
				play.classList.add("replay");
				play.classList.remove("pause");
			};
			
			// Updating progressbar
			audio.ontimeupdate = function() {
				progress.max = audio.duration;
				progress.value = audio.currentTime;
			};
			
			// Controlling playback by progressbar
			progress.onclick = function(e) {
				var value = e.layerX / progress.offsetWidth * audio.duration;
				audio.currentTime = value;
				audio.play();
			};
		})();
	}
	
	
	
	/*******************************************************
	
		Video players
	
	*******************************************************/
	
	var allVideoPlayers = document.querySelectorAll(".player.video");
	for (var i=0; i<allVideoPlayers.length; i++) {
		(function() {
			var player = allVideoPlayers[i];
			
			var thumbnail = player.querySelector(".thumbnail");
			
			thumbnail.onclick = function() {
				if (player.classList.contains("native")) {
					// Native player — just a <video> tag
					var video = document.createElement("video");
					video.src = player.dataset.videoUrl;
					video.controls = true;
					player.appendChild(video);
					video.play();
				}
				else {
					// External player — an <iframe> tag
					var iframe = document.createElement("iframe");
					iframe.src = player.dataset.iframeUrl;
					iframe.frameBorder = 0;
					iframe.setAttribute("allowfullscreen", "true");
					player.appendChild(iframe);
				}
				
				// If current video is under post heading, hide the heading
				var prev = player.previousElementSibling;
				if (prev != null && prev.tagName == "H2")
					prev.classList.add("hidden");
				
				// Hide thumbnail
				player.removeChild(thumbnail);
				
				return false;
			};
		})();
	}
});