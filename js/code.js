document.addEventListener("DOMContentLoaded", function()
{
	var allPreBlocks = document.getElementsByTagName("pre");
	for (var i=0; i<allPreBlocks.length; i++) {
		var pre = allPreBlocks[i];
		var language = pre.dataset.language;
		if (language != undefined && hljs.getLanguage(language) != undefined)
			pre.innerHTML = hljs.highlight(language, pre.textContent).value;
	}
});