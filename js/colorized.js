jQuery(document).ready(function($)
{
	var ajaxDomain = $("head meta[name='ajaxDomain']")[0].content;
	
	$("a.comments-button").click(function() {
		var button = $(this);
		var article = button.parents(".article");
		var pubid = article.data("pubid");
		var content = article.find(".part1 .content");
		var part2 = article.find(".part2");
		var container = part2.find("#replies-"+pubid.replace("/", "\\/"));
		
		if (!part2.hasClass("opened")) {
			// Комментарии пока скрыты. Покажем форму и начнём их подгружать.
			part2.css("maxHeight", part2.prop("height"));
			part2.stop().animate({maxHeight: part2.prop("scrollHeight")}, 300, "linear", function() {
				part2.css("maxHeight", "none");
			});
			part2.addClass("opened");
			content.removeClass("rounded");
			button.addClass("waiting");
			
			// Отправляем запрос
			$.ajax({
				url: "//"+ajaxDomain+"/rfc?pubid="+pubid,
				dataType: "text",
				xhrFields: { withCredentials: true },
				success: function(data) {
					container.html(data);
					
					// Раскрываем то, что только что подгрузили
					part2.css("maxHeight", part2.prop("height"));
					part2.stop().animate({maxHeight: part2.prop("scrollHeight")}, 300, "linear", function() {
						part2.css("maxHeight", "none");
					});
					part2.addClass("opened");
					button.removeClass("waiting");
				}
			});
		}
		else {
			part2.css("maxHeight", part2.prop("scrollHeight"));
			part2.stop().animate({maxHeight: 0}, 300, "linear", function() {
				content.addClass("rounded");
			});
			part2.removeClass("opened");
			button.removeClass("waiting");
		}
		
		return false;
	});
	
	$(".approve-button").click(function() {
		var approveButton = $(this);
		var commentsButton = approveButton.siblings(".comments-button");
		var article = approveButton.parents(".article");
		var pubid = article.data("pubid");
		var textmeta = article.find(".postmeta .textmeta");
		var part2 = article.find(".part2");
		
		approveButton.hide();
		commentsButton.show().addClass("waiting");
		
		$.ajax({
			type: "POST",
			url: "//"+ajaxDomain+"/approve",
			data: { pubid: pubid, showClubLink: true },
			dataType: "json",
			success: function(data) {
				article.removeClass("onmoderation");
				article.data("pubid", data.pubid);
				textmeta.html(data.textmeta);
				commentsButton.removeClass("waiting");
				
				// Раскрываем форму комментариев
				part2.css("maxHeight", part2.prop("height"));
				part2.stop().animate({maxHeight: part2.prop("scrollHeight")}, 300, "linear");
				part2.addClass("opened");
				button.removeClass("waiting");
			}
		});
		
		return false;
	});
	
	$(".tags input[type='text']").keypress(function(e) {
		if (e.which == 13) {
			var newTag = $(this).prop("value");
			
			// Пустой тег добавить нельзя
			if (newTag == "")
				return false;
			
			// Уже существующий тег добавить ещё раз нельзя
			var existingTags = $(this).siblings("span");
			for (var i=0; i<existingTags.length; i++) {
				if (existingTags.eq(i).text() == newTag+" ") {
					// Подссветим найденный тег, чтобы пользователь понял, в чём дело
					existingTags.eq(i).addClass("fadetoyellow");
					setTimeout(function() {
						existingTags.eq(i).removeClass("fadetoyellow");
					}, 1500);
					return false;
				}
			}
			
			var span = $("<span/>")
				.text(newTag)
				.append(" <a href='#'></a>")
				.insertBefore(this)
				.after(" ");
			
			$("<input type='hidden'/>")
				.attr("name", "tags[]")
				.attr("value", newTag)
				.appendTo(span);
			
			// Очищаем поле
			$(this).prop("value", "");
			
			return false;
		}
	});
	
	$(".tags").on("click", "span a", function() {
		var span = $(this).parent();
		span.fadeOut(500);
		setTimeout(function() { span.remove(); }, 500);
		return false;
	});
	
	$(".leftlinks a").click(function() {
		$(this).siblings(".leftlinks-popup").fadeToggle(300);
		return false;
	});
	
	$("form.write textarea").autosize({append:"\n"});
	$("form.reply-form textarea").autosize({append:"\n"});
	
	$(".compare-with-html").click(function() {
		var form = $(this).closest("form");
		form.find("input[name='action']").val("compare-with-html");
		form.attr("target", "_blank");
		form.submit();
		setTimeout(function() {
			form.removeAttr("target");
			form.find("input[name='action']").val("save");
		}, 500);
	});
	
	$("form.reply-form").submit(function() {
		var form = $(this);
		var part2 = form.parents(".part2");
		var button = part2.parent().find(".comments-button");
		var pubid = form.find("input[name='parent']").val();
		
		// Indicate that the process was started
		button.addClass("waiting");
		
		// Send data to server
		var formData = form.serializeArray();
		formData[formData.length] = {name:"ajax", value:"1"};
		$.ajax({
			type: "POST",
			url: "//"+ajaxDomain+"/reply",
			data: formData,
			dataType: "text",
			success: function(data) {
				// Append the formed HTML to the parent's replies block
				var parent = part2.find("#replies-"+pubid.replace(/\//g, "\\/"));
				parent.append(data);
				
				// Increase the comments count
				var counter = button.find("span");
				counter.html( parseInt(counter.html()) + 1 );
				button.removeClass("waiting");
				
				// Hide the form (except when it's the root-level form)
				if (form.data("copy") == "true")
					form.hide();
				else
					// For root-level form, just clean the inputs
					form.find("textarea")[0].value = "";
					form.find("input[type='checkbox']")[0].checked = false;
				
				return false;
			},
			error: function(jqXHR, textStatus, errorThrown) {
				// On failure, show the error message
				var msg = form.find(".msg");
				msg.html(jqXHR.responseText);
				msg.add(msg.next()).fadeIn();
				button.removeClass("waiting");
				
				return false;
			}
		});
		
		return false;
	});
	
	$(".msg-close").click(function() {
		$(this).hide();
		$(this).prev().fadeOut(function() { $(this).html(""); });
	})
	
	$(document).delegate(".react-reply", "click", function() {
		var parent = $(this).parents(".reply").first();
		var subreplies = parent.find(".replies").first();
		
		// Create the form if not created here
		if (subreplies.children(".pseudo").length == 0) {
			// Copy the form and place it under current reply
			var form = parent.parents(".part2").find(".reply-form").first().clone(true).hide();
			var wrappedForm = $("<div class='reply pseudo'/>").append(form);
			subreplies.prepend(wrappedForm);
			
			// Autosizing brokes when cloning, so clone the textarea again, without events
			var newTextarea = form.find("textarea").clone();
			newTextarea.attr("style","").autosize({append:"\n"});
			form.find("textarea").replaceWith(newTextarea);
			
			// Fill the hidden field «parent», according to where the form is placed
			form.find("input[name='parent']").val( parent.data("pubid") );
			
			// Indicate that the form is a copy, not the original root-level form
			form.data("copy", "true");
		}
		
		// Toggle visibility of the form
		subreplies.find(".reply-form").first().fadeToggle();
		
		return false;
	});
	
	$("a.love").click(function() {
		var heart = $(this);
		
		if (heart.hasClass("active")) {
			$.post("//"+ajaxDomain+"/unsubscribe", {
				session: $.cookie("session"),
				blog: heart.data("blog")
			});
			heart.removeClass("active");
		}
		else {
			$.post("//"+ajaxDomain+"/subscribe", {
				session: $.cookie("session"),
				blog: heart.data("blog")
			});
			heart.addClass("active");
		}
		
		return false;
	});
	
	$("a#tab-register").click(function() {
		$("#form-login").hide();
		$("#form-register").show();
		$("#tab-login").removeClass("pseudobold").addClass("dotted");
		$("#tab-register").removeClass("dotted").addClass("pseudobold");
		$(".antiroof").hide();
		$(".msg").hide();
		return false;
	});
	
	$("a#tab-login").click(function() {
		$("#form-register").hide();
		$("#form-login").show();
		$("#tab-register").removeClass("pseudobold").addClass("dotted");
		$("#tab-login").removeClass("dotted").addClass("pseudobold");
		$(".antiroof").show();
		$(".msg").hide();
		return false;
	});
});