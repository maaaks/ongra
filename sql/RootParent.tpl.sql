{{% import std.string; }}

WITH RECURSIVE tmp(tmpid, {{ Entry.fields().join(", ") |none}}) AS (
	SELECT 0, {{ Entry.fields().join(", ") }}
	FROM entries
	WHERE id = $1
UNION
	SELECT tmp.tmpid+1, entries.{{ Entry.fields().join(`, entries.`) |none}}
	FROM entries, tmp
	WHERE entries.id = tmp.parent_id
	AND tmp.type != 'Post'  -- we haven't reached the nearest root yet
)
SELECT
{{ Entry.fieldsForSelect(`tmp.`, `entry.`) |none}},
{{ User.fieldsForSelect(`users.`, `entry.user.`) |none}},
{{ Blog.fieldsForSelect(`blogs.`, `entry.user.primaryblog.`) |none}}
FROM tmp
	LEFT JOIN users         ON tmp.author_id = users.id
	LEFT JOIN subscriptions ON users.id = subscriptions.subscriber_id
	LEFT JOIN blogs         ON subscriptions.blog_id = blogs.id
WHERE subscriptions.access = 'Primary'
ORDER BY tmpid DESC
LIMIT 1