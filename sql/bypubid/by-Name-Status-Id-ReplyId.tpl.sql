{{%
struct Data { bool idOnly; }
Data d = loadTplData!Data(_arguments);
}}

WITH RECURSIVE tmp(id, created, content) AS (
	SELECT entries.id, entries.created, entries.content
	FROM entries
	LEFT JOIN blogs ON entries.blog_id=blogs.id
	WHERE blogs.name = $1
	AND entries.space = $2
	AND entries.local_id = $3
	AND entries.type = 'Post'
	AND entries.status != 'Deleted'
UNION
	SELECT entries.id, entries.created, entries.content
	FROM entries, tmp
	WHERE entries.parent_id = tmp.id
	AND entries.status != 'Deleted'
)
SELECT {{?d.idOnly}} tmp.id {{:}} {{ Entry.fieldsForSelect("tmp.", "entry.") |none}} {{/}}
FROM tmp
ORDER BY created
OFFSET $4 LIMIT 1