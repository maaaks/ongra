{{%
struct Data { bool idOnly; }
Data d = loadTplData!Data(_arguments);
}}

SELECT {{?d.idOnly}} entries.id {{:}} {{ Entry.fieldsForSelect("entries.", "entry.") |none}} {{/}}
FROM entries
LEFT JOIN blogs ON entries.blog_id=blogs.id
WHERE blogs.name = $1
AND entries.space = $2
AND entries.type = 'Post'
AND entries.local_id = $3
AND entries.status != 'Deleted'