{{% import ongra; }}

{{% auto pageSize = Config.display.RepliesPageSize; }}

SELECT
{{ User.fieldsForSelect("users.", "user.") |none}},
{{ Entry.fieldsForSelect("entries.", "entry.") |none}},
{{ Blog.fieldsForSelect("prblog.", "user.primaryblog.") |none}},
{{ Blog.fieldsForSelect("blog.", "entry.blog.") |none}},
inbox.unread AS "unread"
FROM inbox
	LEFT JOIN entries       ON entries.id = inbox.entry_id
	LEFT JOIN users         ON users.id = entries.author_id
	LEFT JOIN subscriptions ON subscriptions.subscriber_id = users.id
	LEFT JOIN blogs prblog  ON prblog.id = subscriptions.blog_id
	LEFT JOIN blogs blog    ON blog.id = entries.blog_id
WHERE inbox.subscriber_id = $1
AND subscriptions.access = 'Primary'
ORDER BY entries.created DESC
LIMIT {{pageSize}} OFFSET ($2 - 1) * {{pageSize}}