SELECT users.id
FROM users
RIGHT JOIN auth_classic ON auth_classic.user_id = users.id
WHERE auth_classic.email = $1
AND auth_classic.sha512 = $2
LIMIT 1