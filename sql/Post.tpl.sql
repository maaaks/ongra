{{% import std.string; }}

WITH RECURSIVE tmp(breadcrumbs, level, {{ Entry.fields.join(`, `) |none}}) AS (
	SELECT
		ARRAY[created] AS breadcrumbs,
		0 AS level,
		{{ Entry.fields.join(`, `) }}
	FROM entries
	WHERE id = $1
UNION
	SELECT
		tmp.breadcrumbs || entries.created,
		tmp.level + 1 AS level,
		{{ `entries.` ~ Entry.fields.join(`, entries.`) |none}}
	FROM entries, tmp
	WHERE entries.parent_id = tmp.id
)
SELECT
{{ Entry.fieldsForSelect(`entry.`, `entry.`) |none}},
{{ Blog.fieldsForSelect(`blog.`, `entry.blog.`) |none}},
{{ User.fieldsForSelect(`author.`, `entry.author.`) |none}},
{{ Blog.fieldsForSelect(`user_blog.`, `entry.author.primaryblog.`) |none}},
{{ Upload.fieldsForSelect(`userpic.`, `entry.primaryblog.userpic.`) |none}},
	entry.level AS "level",
	(ROW_NUMBER() OVER(ORDER BY entry.created))-1 AS "entry.pf_seqnumber"
FROM      tmp           AS entry
LEFT JOIN users         AS author    ON entry.author_id = author.id
LEFT JOIN blogs         AS blog      ON entry.blog_id = blog.id
LEFT JOIN subscriptions AS sub       ON author.id = sub.subscriber_id
LEFT JOIN blogs         AS user_blog ON sub.blog_id = user_blog.id
LEFT JOIN uploads       AS userpic   ON user_blog.userpic_id = userpic.id
WHERE (sub.access IS NULL OR sub.access = 'Primary')
AND blog.deleted = false
GROUP BY
	entry.{{     Entry.fields.join(`, entry.`)    |none}},
	blog.{{      Blog.fields.join(`, blog.`)      |none}},
	author.{{    User.fields.join(`, author.`)    |none}},
	user_blog.{{ Blog.fields.join(`, user_blog.`) |none}},
	userpic.{{   Upload.fields.join(`, userpic.`) |none}},
	entry.level, entry.breadcrumbs
ORDER BY entry.breadcrumbs