{{% import std.string; }}

{{% auto pageSize = Config.display.PageSize; }}

SELECT
{{ Entry.fieldsForSelect(`entries.`, `entry.`) |none}},
{{ Blog.fieldsForSelect(`blogs.`, `entry.blog.`) |none}},
count_comments(entries.id) AS "entry.pf_commentscount"
FROM entries
LEFT JOIN blogs ON entries.blog_id = blogs.id
WHERE
	entries.type = 'Post'
	AND entries.status = 'Published'
	AND blogs.deleted = false
GROUP BY
	entries.{{ Entry.fields.join(`, entries.`) |none}},
	blogs.{{ Blog.fields.join(`, blogs.`) |none}}
ORDER BY entries.created DESC
LIMIT {{ pageSize }} OFFSET ($1 - 1) * {{ pageSize }}