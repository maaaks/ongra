SELECT COUNT(*)
FROM editorschoice
LEFT JOIN entries ON entries.id = editorschoice.entry_id
LEFT JOIN blogs   ON blogs.id = entries.blog_id
WHERE entries.status = 'Published'
AND entries.status != 'Deleted'
AND blogs.deleted = false