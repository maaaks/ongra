SELECT COUNT(entries.id)
FROM entries
RIGHT JOIN subscriptions ON entries.blog_id = subscriptions.blog_id
LEFT JOIN blogs ON entries.blog_id = blogs.id
WHERE
	subscriptions.subscriber_id = $1
	AND subscriptions.access != 'Invited'
	AND entries.type = 'Post'
	AND entries.status = 'Published'
	AND entries.author_id != $1 -- not to read myself
	AND blogs.deleted = false