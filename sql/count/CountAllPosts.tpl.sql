SELECT COUNT(entries.id)
FROM entries
LEFT JOIN blogs ON entries.blog_id = blogs.id
WHERE entries.type = 'Post'
AND entries.status = 'Published'
AND blogs.deleted = false