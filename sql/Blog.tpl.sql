{{% import std.string; }}

{{%
struct Data {
	bool onlyCount;   // set to true if you need only to count entries, not to load them
	bool filterByTag; // set to true if you need to filter entries by tag
}
Data d = loadTplData!Data(_arguments);
}}

{{!
	RUNTIME ARGUMENTS
	$1 — name of blog to show
	$2 — id of viewer (usually App.ram.user_id)
	$3 — page number
	$4 — tag (if filterByTag is true)
	$5 — tag (if filterByTag is true), with spaces replaced with underscores
}}

{{? d.onlyCount }}
SELECT COUNT(entries.id)
{{:}}
SELECT {{ Entry.fieldsForSelect(`entries.`, `entry.`) |none}},
count_comments(entries.id) AS "entry.pf_commentscount"
{{/}}
FROM entries
LEFT JOIN blogs ON entries.blog_id = blogs.id
LEFT JOIN subscriptions ON subscriptions.blog_id = blogs.id AND subscriptions.subscriber_id = $2
{{? d.filterByTag }}
LEFT JOIN tags ON entries.id = tags.entry_id
LEFT JOIN tag ON tags.tag_id = tag.id
{{/}}
WHERE
	blogs.name = $1
	AND entries.type = 'Post'
	AND $3 = $3 -- just to make sure that $3 is always used
	AND ((
		-- Draft can be accessed by its author only
		entries.status = 'Draft'
		AND entries.author_id = $2
	) OR (
		-- Entry on moderation can be both viewed and edited by author or moderator
		entries.status = 'OnModeration'
		AND (entries.author_id = $2 OR subscriptions.access >= 'Moderator')
	) OR (
		-- Public entries can be viewed by anyone
		entries.status = 'Published'
	))
	{{? d.filterByTag }}
	AND tag.name IN ($4, $5)
	{{/}}
{{?! d.onlyCount }}
GROUP BY
	entries.{{ Entry.fields.join(`, entries.`) }}
ORDER BY entries.created DESC
LIMIT {{ Config.display.PageSize }}
OFFSET ($3::integer - 1) * {{ Config.display.PageSize }}
{{/}}