{{! RUNTIME ARGUMENTS }}
{{! $1 — id of the blog to count followers for }}

SELECT COUNT(subscriber_id)
FROM subscriptions
LEFT JOIN blogs ON blogs.id=subscriptions.blog_id
WHERE blogs.id = $1
AND (blogs.type != 'Personal' OR subscriptions.subscriber_id != blogs.owner_id)
AND subscriptions.access != 'Invited'