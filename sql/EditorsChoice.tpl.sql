{{% import ongra; }}

{{% auto pageSize = Config.display.RepliesPageSize; }}

SELECT
{{ Entry.fieldsForSelect(`entries.`, `entry.`) |none}},
{{ User.fieldsForSelect(`users.`,    `entry.author.`) |none}},
{{ Blog.fieldsForSelect(`blogs.`,    `entry.blog.`) |none}},
count_comments(entries.id) AS "entry.pf_commentscount"
FROM editorschoice
LEFT JOIN entries ON entries.id = editorschoice.entry_id
LEFT JOIN users   ON users.id = entries.author_id
LEFT JOIN blogs   ON blogs.id = entries.blog_id
WHERE entries.status = 'Published'
AND entries.status != 'Deleted'
AND blogs.deleted = false
ORDER BY editorschoice.id DESC
LIMIT {{pageSize}} OFFSET ($1 - 1) * {{pageSize}}