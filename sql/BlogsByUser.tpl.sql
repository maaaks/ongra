SELECT
	subscriptions.access   AS "access",
	blogs.id               AS "blog.id",
	blogs.name             AS "blog.name",
	blogs.title            AS "blog.title",
	blogs.type             AS "blog.type",
	blogs.userpic_id       AS "blog.userpic_id",
	uploads.directory      AS "blog.userpic.directory",
	uploads.filename       AS "blog.userpic.filename",
	users.id               AS "primaryblog.owner_id",
	users.id               AS "primaryblog.owner.id",
	users.nick             AS "primaryblog.owner.nick",
	primaryblog.id         AS "primaryblog.id",
	primaryblog.name       AS "primaryblog.name",
	primaryblog.type       AS "primaryblog.type",
	primaryblog.userpic_id AS "primaryblog.userpic_id",
	uploads2.directory     AS "primaryblog.userpic.directory",
	uploads2.filename      AS "primaryblog.userpic.filename"
FROM blogs
LEFT JOIN subscriptions        ON subscriptions.blog_id = blogs.id
LEFT JOIN users                ON users.id = subscriptions.subscriber_id
LEFT JOIN subscriptions AS _s  ON _s.subscriber_id = users.id
LEFT JOIN blogs AS primaryblog ON primaryblog.id = _s.blog_id
LEFT JOIN uploads              ON uploads.id = blogs.userpic_id
LEFT JOIN uploads AS uploads2  ON uploads2.id = primaryblog.userpic_id
WHERE _s.access = 'Primary'
AND primaryblog.name = $1
AND subscriptions.access >= $2
AND blogs.deleted = false