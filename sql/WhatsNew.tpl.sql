{{% import std.string; }}

{{% auto pageSize = Config.display.PageSize; }}

SELECT
{{ Entry.fieldsForSelect(`entries.`, `entry.`) |none}},
count_comments(entries.id) AS "entry.pf_commentscount"
FROM entries
RIGHT JOIN subscriptions ON entries.blog_id = subscriptions.blog_id
LEFT JOIN blogs ON entries.blog_id = blogs.id
WHERE
	subscriptions.subscriber_id = $1
	AND subscriptions.access != 'Invited'
	AND entries.type = 'Post'
	AND entries.status = 'Published'
	AND entries.author_id != $1 -- not to read myself
	AND blogs.deleted = false
GROUP BY
	entries.{{ Entry.fields.join(`, entries.`) |none}}
ORDER BY entries.created DESC
LIMIT {{pageSize}} OFFSET ($2 - 1) * {{pageSize}}