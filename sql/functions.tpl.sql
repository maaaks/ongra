CREATE OR REPLACE FUNCTION count_comments(given_id bigint) RETURNS bigint AS
$$
	WITH RECURSIVE roots(id, root_id) AS (
		SELECT id, id FROM entries
		WHERE id=given_id
	UNION
		SELECT entries.id, roots.root_id FROM entries, roots
		WHERE entries.parent_id = roots.id
		AND entries.status != 'Deleted'
	)
	SELECT COUNT(id) FROM roots
	WHERE id != root_id
$$
LANGUAGE SQL;