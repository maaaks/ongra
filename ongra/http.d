module ongra.http;

import std.conv;
import vibe.http.status;

/**
	Base class for any Exception that is not a redirect.
	An exception may have a custom title and text to be shown on the page.
	Throwable.msg is used for storing title.
	
	Do not use directly. Use Http instead.
 */
class HttpException : Exception
{
	int code;
	string message;
	
	private this(in int code, in string title, in string message) {
		super(title);
		this.code = code;
		this.message = message;
	}
	
	/// Make "msg" conveniently accessible as "title"
	@property string title() const { return msg; }
	@property void title(in string title) { msg = title; }
}


/**
	Template for non-redirect exceptions.
	
	Usage:
		throw new Http!500("I don't like you", "Server doesn't want to reply to you.");
		throw new Http!500("Server doesn't want to reply to you."); // with default title ("500 Internal Server Error")
 */
class Http(int Code) : HttpException
if (Code!=201 && Code!=202 && !(300<=Code && Code<=308))
{
	this() {
		super(Code, Code.to!string~" "~httpStatusText(Code), httpStatusText(Code));
	}
	
	this(in string message) {
		super(Code, Code.to!string~" "~httpStatusText(Code), message);
	}
	
	this(in string title, in string message) {
		super(Code, title, message);
	}
}


/**
	Base class for any Exception that is a redirect to new location.
	Must have a HTTP code and the location to go to.
	Throwable.msg is used for storing location.
	
	Do not use directly. Use Http instead.
 */
class HttpRedirect : Exception
{
	int code;
	
	private this(in int code, in string location) {
		this.code = code;
		super(location);
	}
	
	/// Make "msg" conveniently accessible as "location"
	@property string location() const { return msg; }
	@property void location(in string location) { msg = location; }
}


/**
	Template for redirects.
	Default HTTP code is 302 Moved Temporarily.
	
	Usage:
		throw new Http("https://www.yandex.ru/"); // 302
		throw new Http!301("https://www.yandex.ru/"); // 301
 */
class Http(int Code=302) : HttpRedirect
if (Code==201 || Code==202 || (300<=Code && Code<=308))
{
	this(in string location) {
		super(Code, location);
	}
}