module ongra.myrouter;

import curtain.router;
import ongra;
import std.array;
import std.conv;
import std.format;
import std.math;
import std.regex;
import std.typecons;
import vibe.http.server;
import vibe.http.status;
import vibe.inet.url;


class MyRouter : HTTPServerRequestHandler
{
	private Router!(void function()) realRouter;
	
	void handleRequest(HTTPServerRequest request, HTTPServerResponse response) {
		// When the first request is processed, initialize the Router object.
		// This could be done in constructor, but static Config.routes aren't availiable yet then,
		// because MyRouter's this() is called from App's static this().
		if (realRouter is null) {
			realRouter = new Router!(void function());
			
			// Fill the Router object with «route — callback» pairs
			foreach (routeName; __traits(allMembers, Config.struct_routes)) {
				Route route = __traits(getMember, Config.routes, routeName);
				void function() callback = () { mixin("action_"~routeName~"();"); };
				realRouter.addRoute(route, callback);
			}
		}
		
		try {
			App.request = request;
			App.response = response;
			App.rt = App.rt.init;
			App.rt.ajaxMode = request.form.get("ajax", "false") != "false";
			Ram.initAppRam();
			
			response.contentType = "text/html; charset=utf-8";
			runAction(request, response);
		}
		catch (HttpRedirect e) {
			response.redirect(e.location, e.code);
		}
		catch (HttpException e) {
			if (App.rt.ajaxMode) {
				response.statusCode = e.code;
				response.contentType = "text/plain; charset=utf-8";
				response.bodyWriter.write(e.message);
			}
			else {
				response.statusCode = e.code;
				response.contentType = "text/html; charset=utf-8";
				App.rt.errorMode = true;
				App.rt.title = say!"errors/http error title".format(e.message);
				App.rt.titleHtml = e.title;
				App.msgBox(e.title, e.message);
			}
		}
		catch (Throwable e) {
			App.log(e);
			if (App.rt.ajaxMode) {
				response.statusCode = 500;
				response.contentType = "text/plain; charset=utf-8";
				response.bodyWriter.write("500 Internal Server Error");
			}
			else {
				response.statusCode = 500;
				response.contentType = "text/html; charset=utf-8";
				App.rt.errorMode = true;
				App.rt.title = say!"errors/http error title";
				App.rt.titleHtml = "500 Internal Server Error";
				App.msgBox("500 Internal Server Error", "Internal Server Error");
			}
		}
	}
	
	private void runAction(HTTPServerRequest request, HTTPServerResponse response) {
		Route.MatchResult res = realRouter.getRoute(request.host ~ request.path);
		if (!res)
			throw new Http!500("Strange URL happened.");
		
		final switch (res.status) {
			case Route.MatchStatus.MatchesPerfectly:
				App.activeRoute = res.route;
				foreach (key, value; res.params)
					request.params[key] = value;
				realRouter.getData(res.route)();
				return;
				
			case Route.MatchStatus.NeedToAppendSlash:
				URL url = App.request.fullURL();
				url.path = Path(url.path.toString()~"/");
				url.port = 80;
				throw new Http!302(url.toString());
				
			case Route.MatchStatus.NeedToRemoveSlash:
				URL url = App.request.fullURL();
				url.path = Path(url.path.toString()[0..$-1]);
				url.port = 80;
				throw new Http!302(url.toString());
				
			case Route.MatchStatus.DoesNotMatch:
				throw new Http!500("Very-very strange URL happened.");
		}
	}
	
	string makeLink(Blog blog) {
		if (blog.name.match(`^ya:`))
			return App.url!"PassiveBlog"(["name": blog.name[3..$] ]);
		else
			return App.url!"Blog"(["name": blog.name]);
	}
	
	string makeLink(User user) {
		return user.primaryBlog
			? makeLink(user.primaryBlog)
			: App.url!"Main" ;
	}
	
	string makeLink(Entry entry, Entry rootEntry=null) {
		if (entry.type == Entry.Type.Post && !rootEntry) {
			if (entry.blog.name.match(`^ya:`))
				return App.url!"PassiveBlogPost"([
					"name":     entry.blog.name[3..$],
					"local_id": entry.local_id.to!string,
				]);
			else final switch (entry.space) {
				case Entry.Space.Default:
					return App.url!"Post"([
						"name":     entry.blog.name,
						"local_id": entry.local_id.to!string,
					]);
				case Entry.Space.Draft:
					return App.url!"PostDraft"([
						"name":     entry.blog.name,
						"local_id": entry.local_id.to!string,
					]);
			}
		}
		else if (rootEntry)
			return makeLink(rootEntry) ~ "#" ~ entry.anchor(rootEntry);
		else
			return makeLink(entry.rootParent()) ~ "#" ~ entry.anchor(entry.rootParent());
	}
	
	string makeEditLink(Entry entry) {
		string[string] params = [
			"name":     entry.blog.name,
			"local_id": entry.local_id.to!string,
		];
		
		final switch (entry.space) {
			case Entry.Space.Default:
				return App.url!"EditPost"(params);
				
			case Entry.Space.Draft:
				return App.url!"EditPostDraft"(params);
		}
	}
}
