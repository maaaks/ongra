module ongra.models.auth;

import curtain.activerecord;
import ongra;
import std.conv;
import std.datetime;
import std.digest.sha;
import std.string;

class Auth_Classic
{
	mixin ARId!(string, "email");
	mixin ARField!(DateTime, "created");
	mixin ARField!(string, "sha512");
	mixin ARField!(string, "activation_code");
	mixin ARHasOne!(ulong, "user_id", User, "user");
	
	mixin AR!"auth_classic";
	
	this() {
		created = SysTime(Clock.currStdTime()).to!DateTime;
	}
	
	static string hash(in string password) {
		return digest!SHA512(password).toHexString().toLower();
	}
}

class Auth_Yandex
{
	mixin ARId!(string, "login");
	mixin ARField!(DateTime, "created");
	mixin ARField!(bool, "active");
	mixin ARHasOne!(ulong, "user_id", User, "user");
	
	mixin AR!"auth_yandex";
	
	this() {
		created = SysTime(Clock.currStdTime()).to!DateTime;
	}
}