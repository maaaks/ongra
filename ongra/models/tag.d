module ongra.models.tag;

import curtain.activerecord;
import ongra;
import std.array;

class Tag
{
	mixin ARId!(ulong);
	mixin ARField!(string, "name");
	
	mixin AR;
	
	this(in string name="") {
		this.name = name;
	}
	
	static Tag fromName(in string name) {
		Tag obj = Tag.find("name = '"~name.replace(`'`,`''`)~"'");
		if (!obj) {
			obj = new Tag(name);
			obj.save();
		}
		return obj;
	}
}