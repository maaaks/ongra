module ongra.models.upload;

import curtain.activerecord;
import ongra;
import std.conv;
import std.file;
import std.path;
import std.process : spawnProcess;
import vibe.inet.webform;

class Upload
{
	mixin ARId;
	mixin ARField!(string, "directory");
	mixin ARField!(string, "filename");
	mixin ARHasOne!(ulong,"entry_id", Entry,"entry");
	mixin ARHasOne!(ulong,"user_id",  User,"user");
	mixin AR!"uploads";
	
	private string _relativePath() {
		return "/uploads/"~directory~"/"~filename;
	}
	
	/// Returns physical local path of the file.
	string fullLocalPath() {
		return Config.server.StaticDir~_relativePath();
	}
	
	/// Returns URL ready to be inserted into a page.
	string url() {
		return App.url!"Static"~_relativePath();
	}
	
	
	/**
		Creates new Upload from a temporary file, moving it under /uploads directory.
	 */
	static Upload create(in FilePart file, User user, Entry entry=null) {
		if (file.filename.to!string.length == 0)
			return null;
		
		// Create an Upload object, fill its main fields
		Upload upload = new Upload;
		upload.filename = file.filename.to!string;
		upload.user = user;
		upload.entry = entry;
		
		// Generate random string until it is unique
		while (true) {
			try {
				upload.directory = generateRandomString!8 ~ '/' ~ generateRandomString!8;
				upload.save(); // will throw exception here when string isn't unique
				break;
			}
			catch (Db.DbException e) {}
		}
		
		// Move file to where it should be found by server.
		// If upload.fullLocalPath is not on the same logical device as /tmp,
		// then moving is not possible, and so we use copying instead.
		mkdirRecurse(dirName(upload.fullLocalPath));
		try {
			std.file.rename(file.tempPath.to!string, upload.fullLocalPath);
		}
		catch(FileException e) {
			// std.file.copy() has a namesake in std.algorithm,
			// and std.file.remove() has a namesake in my ActiveRecord.
			std.file.copy(file.tempPath.to!string, upload.fullLocalPath);
			std.file.remove(file.tempPath.to!string);
		}
		
		// Make the file readable by everyone
		spawnProcess(["chmod", "644", upload.fullLocalPath]);
		
		return upload;
	}
}