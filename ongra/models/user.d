module ongra.models.user;

import curtain.activerecord;
import derelict.pq.pq;
import ongra;
import std.algorithm.iteration;
import std.array;
import std.conv;
import std.datetime;
import std.file;
import std.net.curl;
import std.path;
import std.string;

class User
{
	mixin ARId!(ulong);
	mixin ARField!(string,   "nick");
	mixin ARField!(DateTime, "created");
	mixin AR!"users";
	
	private Blog _primaryBlog;
	
	this() {
		created = SysTime(Clock.currStdTime()).to!DateTime;
	}
	
	
	/**
		Load primaryBlog if it exists in the query results.
	 */
	private void afterLoad(Row row, in string prefix) {
		if (row.hasColumn(prefix~"primaryblog.id"))
			_primaryBlog = Blog.fromRow(row, prefix~"primaryblog.");
	}
	
	
	/**
		Returns true if current user is subscribed to blog with given id.
	 */
	bool follows(ulong blogId) {
		auto result = Db.execSqlAndGet!ulong(
			`SELECT COUNT(*) FROM subscriptions WHERE subscriber_id=$1 AND blog_id=$2 AND access!=$3`,
			this.id, blogId, Blog.Access.Invited);
		return (result != 0);
	}
	
	
	/**
		Returns true if current user is subscribed to given blog.
	 */
	bool follows(Blog blog) {
		return follows(blog.id);
	}
	
	
	/**
		Returns number of blogs followed by current user.
		
		See also: Blog.numOfFollowers().
	 */
	size_t numOfFriends() {
		return Db.execSqlAndGet!size_t(
			"SELECT COUNT(*) FROM subscriptions WHERE subscriber_id=$1 AND access!=$2", id, Blog.Access.Invited);
	}
	
	
	Blog.Access getSubscription(Blog blog) {
		if (blog.owner_id == this.id)
			return Blog.Access.Admin;
		else {
			PGresult* res = Db.execSql(`SELECT access FROM subscriptions WHERE subscriber_id=$1 AND blog_id=$2`,
				id, blog.id);
			return (PQntuples(res) == 1)
				? Row(res).get!(Blog.Access)(0)
				: Blog.Access.None;
		}
	}
	
	
	Blog primaryBlog() {
		if (_primaryBlog is null) {
			_primaryBlog = Blog.find(`id IN (SELECT blog_id FROM subscriptions WHERE subscriber_id=$1 AND access=$2)`,
				this.id, Blog.Access.Primary);
		}
		return _primaryBlog;
	}
	
	
	/// See also: Blog.userpicUrl()
	string primaryUserpicUrl() {
		auto blog = primaryBlog();
		if (blog !is null && !blog.isnull_userpic_id()) {
			return blog.userpic.url;
		}
		else {
			string userpicsDir = Config.server.StaticDir~"/uploads/"~Config.server.DefaultUserpicsDir;
			DirEntry[] userpics = dirEntries(userpicsDir, SpanMode.shallow).filter!(a => a.isFile).array;
			auto avatarId = id % userpics.length + 1;
			return App.url!"Static"~"/uploads/"~Config.server.DefaultUserpicsDir~"/"~userpics[avatarId].baseName;
		}
	}
}