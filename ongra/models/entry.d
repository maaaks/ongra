module ongra.models.entry;

public import ongra;

import curtain.activerecord;
import derelict.pq.pq;
import liara.parser;
import ongra;
import std.algorithm;
import std.array;
import std.conv;
import std.datetime;

class Entry
{
	// Post type: a «root» post or a comment
	enum Type {
		Post,
		Comment,
	}
	
	// Format of the post data
	enum Format {
		Normal,
		Photo,
		Video,
		Link,
		Review,
		Subscribe,
		Unsubscribe,
		_JoinClub,
		_UnjoinClub,
		Status,
		Answer,
	}
	
	enum Syntax {
		Html,
		Liara,
	}
	
	/**
		Entry's status of publication.
		(Not meant to be compared as integers.)
	 */
	enum Status {
		Draft,
		OnModeration,
		Published,
		Deleted,
	}
	
	enum Space {
		Default,
		Draft,
	}
	
	mixin ARId!(ulong);
	mixin ARField!(long,     "local_id");
	mixin ARField!(DateTime, "created");
	mixin ARField!(Type,     "type");
	mixin ARField!(Format,   "format");
	mixin ARField!(string,   "title");
	mixin ARField!(string,   "content");
	mixin ARField!(Syntax,   "syntax");
	mixin ARField!(Status,   "status");
	mixin ARField!(Space,    "space");
	mixin ARHasOne!(ulong, "parent_id", Entry, "parent");
	mixin ARHasOne!(ulong, "author_id", User,  "author");
	mixin ARHasOne!(ulong, "blog_id",   Blog,  "blog");
	
	mixin ARPseudoField!(ulong, "pf_commentsCount");
	mixin ARPseudoField!(ulong, "pf_seqnumber");
	
	mixin AR!"entries";
	
	private string[] _oldTags = null;
	private string[] _newTags = null;
	LiaraResult _parseResult = null;
	
	Entry[] children;
	
	this() {
		created = SysTime(Clock.currStdTime()).to!DateTime;
		syntax = Syntax.Liara;
	}
	
	@property string toHtml() {
		final switch (syntax) {
			case Syntax.Html:
				return content.replace("\n", "<br/>");
			
			case Syntax.Liara:
				if (_parseResult is null)
					_parseResult = parseLiara(content, new EntryParserParams(this.blog));
				return _parseResult.htmlOutput;
		}
	}
	
	@property string toPlain() {
		final switch (syntax) {
			case Syntax.Html:
				return content;
				
			case Syntax.Liara:
				if (_parseResult is null)
					_parseResult = parseLiara(content, new EntryParserParams(this.blog));
				return _parseResult.plainOutput;
		}
	}
	
	@property OpeningType openingType() {
		final switch (syntax) {
			case Syntax.Html:
				return OpeningType.Normal;
				
			case Syntax.Liara:
				if (_parseResult is null)
					_parseResult = parseLiara(content, new EntryParserParams(this.blog));
				return _parseResult.openingType;
		}
	}
	
	@property string[string] meta() {
		string[string] array;
		foreach (meta; Meta.findAll("entry_id="~id.to!string))
			array[meta.key] = meta.value;
		return array;
	}
	
	void addMeta(string key, string value) {
		Meta meta = new Meta;
		meta.entry_id = id;
		meta.key = key;
		meta.value = value;
		meta.save();
	}
	
	string[] tags(bool returnOld=false) {
		if (_newTags && !returnOld)
			return _newTags;
		
		if (_oldTags is null) {
			_oldTags = [];
			auto result = Db.execSql(`
				SELECT DISTINCT name
				FROM tag
				LEFT JOIN tags ON tags.tag_id = tag.id
				LEFT JOIN entries ON tags.entry_id = entries.id
				WHERE entries.id = $1::integer`, id);
			foreach (i; 0..PQntuples(result))
				_oldTags ~= Row(result,i).get!string("name");
		}
		
		return _oldTags;
	}
	
	void tags(in string[] tags) {
		_newTags = tags.dup;
	}
	
	/**
		Before saving a new post, choose a local_id for it.
	 */
	protected bool beforeSave() {
		if (!_saved && !local_id && type==Type.Post) {
			auto res = Db.execSql(`SELECT COALESCE(MAX(local_id)+1,1) FROM entries WHERE blog_id=$1 AND space=$2`,
				blog_id, space);
			local_id = Row(res).get!long(0);
			if (local_id <= 0)
				local_id = 1;
		}
		return true;
	}
	
	/**
		After an entry is saved, we must update relations with tags.
	 */
	protected void afterSave() {
		string[] oldTags = tags(true);
		string[] newTags = tags();
		
		// Remove some tags
		foreach (tag; oldTags)
			if (!newTags.canFind(tag))
				Db.execSql(`DELETE FROM tags WHERE entry_id=$1 AND tag_id=$2`, this.id, Tag.fromName(tag).id);
		
		// Add some tags
		foreach (tag; newTags)
			if (!oldTags.canFind(tag))
				Db.execSql(`INSERT INTO tags(entry_id, tag_id) VALUES($1, $2)`, this.id, Tag.fromName(tag).id);
	}
	
	
	/**
		Returns pubid or a shorter identifier which may be used as an anchor on an entry's page.
		
		While API calls always require full pubids, anchors need to be shorter and more readable.
		To create an anchor, you pass to this method «omitEntry» — the entry which is «root» on current page.
		For a comment which is close relative to omitEntry (its rootParent is the same entry as omitEntry),
		the first two sections will be omitted and «re» prefix will be added,
		so that URL will look like «ongra.net/john/123#re4» instead of ugly «ongra.net/john/123#john-123-4».
		
		If an entry is a Post (published as a reply to a post or comment), it won't have a short anchor.
		For such post and all its children, the full pubids will be used.
	 */
	string anchor(Entry omitEntry=null) {
		if (type == Type.Comment && (!omitEntry || omitEntry.id == rootParent().id))
			return "re" ~ seqnumber.to!string;
		else
			return pubid(this);
	}
	
	Entry rootParent() {
		auto result = Db.execSql(renderSql!"RootParent", id);
		return Entry.fromRow(Row(result));
	}
	
	/**
		For a comment, it returns sequence number of this comment (the last part of pubid).
		For a post, always returns 0.
	 */
	ulong seqnumber() {
		if (type == Type.Comment && pf_seqnumber == 0) {
			// It's a comment, and it needs a seqnumber
			// Load all comments for its rootParent, enumerated
			auto result = Db.execSql(renderSql!"Post", rootParent().id);
			
			// Find current entry in the list and get its seqnumber
			foreach (i; 0..PQntuples(result)) {
				auto row = Row(result, i);
				if (row.get!long("entry.id") == id)
					pf_seqnumber = row.get!ulong("entry.pf_seqnumber");
			}
		}
		
		return pf_seqnumber;
	}
	
	string htmlMeta(bool showClub=true) {
		string html = author.primaryBlog.htmlLink();
		
		if (format == Format.Review) {
			html ~= ` в отзывах <a href="`~meta["link"]~`">на Яндекс.Маркете</a>`;
			html ~= ", " ~ created.to!string;
		}
		else if (status != Status.OnModeration) {
			if (showClub && blog_id != author.primaryBlog.id)
				html ~= " в клубе " ~ blog.htmlLink();
			html ~= ", " ~ created.to!string;
		}
		
		if (parent_id)
			html ~= (parent.type == Type.Post)
				? ", в ответ на <a href=\"" ~ App.router.makeLink(parent) ~ "\">пост</a>"
				: ", в ответ на <a href=\"" ~ App.router.makeLink(parent) ~ "\">комментарий</a>";
		
		if (status == Status.OnModeration)
			html ~= showClub
				? ", ожидает подтверждения в клубе " ~ blog.htmlLink()
				: ", ожидает подтверждения";
		
		return html;
	}
}