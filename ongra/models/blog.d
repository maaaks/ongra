module ongra.models.blog;

import curtain.activerecord;
import ongra;
import std.algorithm.iteration;
import std.datetime;
import std.file;
import std.path;
import std.range;

class Blog
{
	/**
		Modes of access to a blog.
		(Can be compared as integers: it's always better when it's bigger.)
	 */
	enum Access {
		None,
		Invited,
		Read,
		ReadWrite,
		Moderator,
		Admin,
		Primary,
	}
	
	enum Type {
		Personal,
		Premoderated,
		Public,
	}
	
	mixin ARId!(ulong);
	mixin ARField!(string,   "name");
	mixin ARField!(string,   "title");
	mixin ARField!(DateTime, "created");
	mixin ARField!(ulong,    "realid");
	mixin ARField!(string,   "description");
	mixin ARField!(bool,     "fromya");
	mixin ARField!(Type,     "type");
	mixin ARField!(bool,     "deleted");
	mixin ARHasOne!(ulong,"userpic_id", Upload,"userpic");
	mixin ARHasOne!(ulong, "owner_id", User, "owner");
		
	mixin AR!"blogs";
	
	this() {
		type = null;
	}
	
	/**
		Returns number of users that follow current blog.
		
		See also: User.numOfFriends().
	 */
	size_t numOfFollowers() {
		return Db.execSqlAndGet!size_t(renderSql!"NumOfFollowers", this.id);
	}
	
	bool isClub() {
		return type != Type.Personal;
	}
	
	string htmlLink(in bool useNameOnly=false) {
		immutable string label = useNameOnly ? name : title;
		immutable string cssClass = isClub() ? "club" : "user";
		immutable string url = App.router.makeLink(this);
		return `<a class="`~cssClass~`" href="`~url~`">`~label~`</a>`;
	}
	
	
	/// See also: User.primaryUserpicUrl()
	string userpicUrl() {
		if (!this.isnull_userpic_id()) {
			return userpic.url;
		}
		else {
			string userpicsDir = Config.server.StaticDir~"/uploads/"~Config.server.DefaultUserpicsDir;
			DirEntry[] userpics = dirEntries(userpicsDir, SpanMode.shallow).filter!(a => a.isFile).array;
			auto avatarId = id % userpics.length + 1;
			return App.url!"Static"~"/uploads/"~Config.server.DefaultUserpicsDir~"/"~userpics[avatarId].baseName;
		}
	}
}
