module ongra.models.meta;

import curtain.activerecord;
import ongra;

class Meta
{
	mixin ARId!(ulong);
	mixin ARField!(string, "key");
	mixin ARField!(string, "value");
	
	mixin ARHasOne!(ulong, "entry_id", Entry, "entry");
	
	mixin AR;
}