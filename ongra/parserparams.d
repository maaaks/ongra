module ongra.parserparams;

import liara.params;
import liara.parser;
import ongra;
import std.conv;
import std.json;
import std.net.curl;
import std.regex;
import std.xml;
import vibe.http.client;

/**
	Ongra-specific parameters and extensions for Liara parser.
	The constructor requires reference to current blog in order to construct links.
 */
class EntryParserParams: LiaraParams
{
	private Blog currentBlog;
	
	this(Blog currentBlog) {
		this.currentBlog = currentBlog;
	}
	
	/**
		Constructs Link object from given target specification which may be not full.
		
		Valid target specification formats are:
		
		1. Global URL
			https://max.ongra.net/
			https://max.ongra.net/1
			https://maaaks.ru/
			http://example.org/file.txt
			mailto:admin@ongra.net
				Links will be rendered "as is", without any modifications.
				In fact, anything that doesn't match other patterns is considered a global URL.
				
		2. Global URL without scheme
			//max.ongra.net
			//max.ongra.net/1
			//maaaks.ru
				The scheme is assumed to be HTTPS.
				
		3. Global URL with an Ongra blog name instead of domain
			max
			max/7
				In these examples, "max" is converted to "https://max.ongra.net".
				
		4. Path part only
			/
			/7
				Current blog's domain will be automatically used.
				
		5. Empty string
				Means exactly the same as "/" because it's parsed as "empty domain + / + empty path".
				So, a user can write a link to himself as "(())".
				
		For Ongra links, label can be automatically constructed when needLabel=true.
		For links to blogs it will be the blog's name, and for links to entries — the titles.
	 */
	override Link makeLink(string target, bool needLabel) {
		// Target starts with "//", which means "https://"
		if (target.match(ctRegex!`^//`))
			return new Link("https:"~target, "https:"~target);
		
		// Target is an Ongra blog (current blog, if empty).
		// If blog does not exist, construct a link to it, anyway.
		else if (target.match(ctRegex!`^[A-z0-9-_]*/?$`)) {
			Blog blog = target.length ? Blog.find("name=$1", target) : currentBlog;
			return blog
				? new Link(App.router.makeLink(blog),       blog.title, blog.isClub ? "club" : "blog")
				: new Link(App.url!"Blog"(["name":target]), target,     "blog");
		}
		
		// Target is an Ongra blog entry, specified by pubid
		else if (auto m = target.match(ctRegex!`^([A-z0-9-_]+)/([0-9]+)$`)) {
			if (!needLabel) {
				// If we don't have to get label, we can construct the URL immediately
				immutable string url = App.url!"Post"(["name":m.captures[1], "local_id":m.captures[2]]);
				return new Link(url, null);
			}
			else {
				// To get label, we retrieve the Entry from database
				Entry entry = findEntryByPubid(m.captures[1], m.captures[0]);
				return new Link(App.router.makeLink(entry), entry.title);
			}
		}
		
		// Target is a custom URL, so just return it as is
		else
			return new Link(target, target);
	}
	
	/**
		Currently does nothing, just returns the given image URL as is.
	 */
	string processImageUrl(string url) {
		return url;
	}
	
	/**
		Entry point for Ongra's custom block extensions for Liara.
		This currently supports embedding video (YouTube, Vimeo) and music (Jamendo).
	 */
	override string makeBlock(string pluginName, string input, bool addLast, LiaraResult* r) {
		switch (pluginName) {
			case "music":
				return _makeMusicBlock(input, addLast);
			case "video":
				return _makeVideoBlock(input, addLast, r);
			default:
				throw new UnsupportedPluginException(pluginName);
		}
	}
	
	
	
	/////////////////////////////////////////////////////////////////////////
	//
	// Private functions
	//
	/////////////////////////////////////////////////////////////////////////
	
	
	/////////////////////////////////////////////////////////////////////////
	// Music
	
	private string _makeMusicBlock(string input, bool addLast) {
		if (auto m = input.match(ctRegex!`^https?://(?:www\.)?jamendo\.com/(?:\w+/)?track/(\d+)`))
			return _getMusicFromJamendo(m.captures[1], addLast);
		
		// By default, just render the original source
		return "&music "~encode(input);
	}
	
	private string _getMusicFromJamendo(string id, bool addLast) {
		// Load data through Jamendo API
		immutable string url = "https://api.jamendo.com/v3.0/tracks/?format=json"
			~ "&client_id=" ~ Config.external.JamendoClientId ~ "&id=" ~ id;
		immutable JSONValue data = parseJSON(get(url))["results"][0];
		
		immutable string songName        = data["name"].str();
		immutable string songPageUrl     = data["shareurl"].str();
		immutable string songStreamUrl   = data["audio"].str();
		immutable string songDownloadUrl = data["audiodownload"].str();
		immutable string artistName      = data["artist_name"].str();
		immutable string artistUrl       = "https://www.jamendo.com/artist/" ~ data["artist_id"].str();
		immutable string coverUrl        = data["album_image"].str();
		
		return `<div class="player audio`~(addLast?" last":"")~`">`
		~	`<img src="`~coverUrl~`" alt=""/>`
		~	`<a class="title" href="`~songPageUrl~`">`~songName~`</a>`
		~	`<div class="artist">`
		~		`<a href="`~artistUrl~`">`~artistName~`</a> at Jamendo`
		~	`</div>`
		~	`<div class="controls">`
		~		`<span class="button play" href="#"></span>`
		~		`<audio src="`~songStreamUrl~`"></audio>`
		~		`<div class="progress"><progress max="262" value="0"></progress></div>`
		~		`<a class="button download" href="`~songDownloadUrl~`"></a>`
		~	`</div>`
		~`</div>`;
	}
	
	
	/////////////////////////////////////////////////////////////////////////
	// Video
	
	private string _makeVideoBlock(string input, bool addLast, LiaraResult* r) {
		string[] args = input.split(ctRegex!`\s+`);
		ulong width;
		ulong height;
		string preview;
		
		// Parse arguments
		for (auto i=1; i<args.length; i++) {
			switch (args[i]) {
				default:
					break;
				
				// Custom size for the video
				case "-size":
					// Size works only when it has a valid size argument after it
					if (i+1 >= args.length)
						break;
					
					// Parse following argument as WxH (e.g. "1280x720")
					if (auto m = args[++i].match(ctRegex!`^(\d+)x(\d+)$`)) {
						width = m.captures[1].to!ulong;
						height = m.captures[2].to!ulong;
					}
					break;
					
				// Custom preview instead of the one provided by the service
				case "-preview":
					// It also works only when it has an argument after it
					if (i+1 >= args.length)
						break;
					
					// Treat the filename/URL of preview the same way as an image filename/URL
					preview = processImageUrl(args[++i]);
					break;
					
				// When specified, should affect the heading (for opening videos only)
				case "-dark":
					r.openingType = OpeningType.DarkWideImage;
					break;
			}
		}
		
		// Now, construct HTML code of embedded videos depending on the exact type of URL
		immutable string url = args[0];
		if (auto m = url.match(ctRegex!
		`^(?:https?://)?(?:(?:www\.|m\.)?youtube\.com/watch\?v=|(?:www\.)?youtu\.be/)([A-z0-9-_]+)`))
			return _getVideoFromYouTube(m.captures[1], width, height, preview, addLast);
		if (auto m = url.match(ctRegex!`^(?:https?://)?vimeo\.com/(\d+)`))
			return _getVideoFromVimeo(m.captures[1], width, height, preview, addLast);
		
		// Fallback: just a link to the unrecognized video
		return "<p"~(addLast?" class=\"last\"":"")~"><a href=\""~encode(url)~"\">"~encode(url)~"</a></p>";
	}
	
	private string _getVideoFromYouTube(string id, ulong width, ulong height, string preview, bool addLast) {
		immutable string url = "https://www.youtube.com/watch?v="~id;
		immutable string iframeUrl = "https://www.youtube.com/embed/"~id~"?autoplay=1&rel=0&showinfo=0";
		
		// Load size information from YouTube, if no custom size provided
		if (!width || !height) {
			immutable string jsonUrl = "https://www.youtube.com/oembed?url=youtube.com/watch?v="~id~"&format=json";
			JSONValue json = parseJSON(get(jsonUrl));
			
			if (!width)  width  = json["width"].uinteger;
			if (!height) height = json["height"].uinteger;
		}
		
		// Guess a good preview, if no custom preview provided
		if (!preview) {
			foreach (size; ["maxresdefault", "sddefault", "mqdefault", "hqdefault", "default"]) {
				try {
					preview = "https://img.youtube.com/vi/"~id~"/"~size~".jpg";
					get(preview);
					break;
				}
				catch (Exception e) {
				}
			}
		}
		
		return _renderVideo(url, iframeUrl, width, height, preview, addLast);
	}
	
	private string _getVideoFromVimeo(string id, ulong width, ulong height, string preview, bool addLast) {
		immutable string url = "https://vimeo.com/"~id;
		immutable string iframeUrl = "https://player.vimeo.com/video/"~id~"?autoplay=1";
		
		// Load data from Vimeo, if neccessary
		if (!width || !height || !preview) {
			immutable string jsonUrl = "https://vimeo.com/api/oembed.json?url=https://vimeo.com/"~id;
			immutable JSONValue json = parseJSON(get(jsonUrl));
			
			if (!width)   width   = json["width"].integer();
			if (!height)  height  = json["height"].integer();
			if (!preview) preview = json["thumbnail_url"].str();
		}
		
		return _renderVideo(url, iframeUrl, width, height, preview, addLast);
	}
	
	private string _renderVideo(
		in string url,
		in string iframeUrl,
		in ulong width,
		in ulong height,
		in string preview,
		in bool addLast
	) {
		return `<div class="wide player video`~(addLast?" last":"")~`"`
		~	` style="padding-top: `~(100.00 * height / width).to!string~`%;"`
		~	` data-iframe-url="`~iframeUrl~`"`
		~`>`
		~	`<a class="thumbnail" href="`~url~`" style="`
		~		`background-image: url('`~preview~`');`
		~		`background-size: cover;`
		~	`"></a>`
		~`</div>`;
	}
}