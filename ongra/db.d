module ongra.db;

import curtain.activerecord;
import derelict.pq.pq;
import ongra;
import std.conv;
import std.stdio;
import std.string;

class Db
{
	static class SqlArg
	{
		private immutable string value;
		private this(string value) {
			this.value = value;
		}
	}
	
	static class DbException : Exception
	{
		this(PGresult* res) {
			immutable string msg = (cast(char*) PQresultErrorMessage(res)).to!string;
			super(msg);
		}
	}
	
	
	/////////////////////////////////////////////////////
	
	
	private static PGconn* connection;
	
	/// Stores how many transactions are already opened, one inside one
	static ulong transactionLevel = 0;
	
	static ulong savepointsCounter = 0;
	
	
	static load() {
		DerelictPQ.load();
		connection = PQconnectdb(("postgresql://"~Config.db.Username~":"~Config.db.Password
			~"@/"~Config.db.DbName).toStringz);
		execSql(renderSql!"functions");
	}
	
	static ~this() {
		PQfinish(connection);
	}
	
	static SqlArg createSqlArg(T)(T value) {
		return new SqlArg(value.to!string);
	}
	
	static PGresult* execSql(T...)(in string sql, T arguments) {
		SqlArg[arguments.length] arr;
		foreach (i, argument; arguments)
			arr[i] = Db.createSqlArg(argument);
		return execSql(sql, arr);
	}
	
	static PGresult* execSql(in string sql, SqlArg[] arguments) {
		if (arguments.length == 0) {
			writeln(sql~"\n");
			auto res = PQexec(connection, sql.toStringz());
			if (PQresultStatus(res) == ExecStatusType.PGRES_FATAL_ERROR
			|| PQresultStatus(res) == ExecStatusType.PGRES_NONFATAL_ERROR)
				_error(res, sql, arguments);
			return res;
		}
		else {
			auto paramTypes   = new Oid[arguments.length];
			auto paramValues  = new const(ubyte)*[arguments.length];
			auto paramLengths = new int[arguments.length];
			auto paramFormats = new int[arguments.length];
			
			foreach (i, argument; arguments) {
				immutable string str = argument.value.to!string;
				auto bytes = cast(const(ubyte)[]) str;
				bytes ~= "\0";
				paramValues[i]  = bytes.ptr;
				paramLengths[i] = bytes.length.to!int;
				paramFormats[i] = 0;
			}
			
			import std.array;
			string readableSql = sql;
			foreach_reverse (i; 0..arguments.length)
				readableSql = readableSql.replace("$"~(i+1).to!string, "'"~arguments[i].value.to!string~"'");
			writeln(readableSql~"\n");
			
			auto res = PQexecParams(connection,
				cast(const(char)*) sql.toStringz(),
				arguments.length.to!int,
				null,
				paramValues.ptr,
				paramLengths.ptr,
				paramFormats.ptr,
				0);
			if (PQresultStatus(res) == ExecStatusType.PGRES_FATAL_ERROR
			|| PQresultStatus(res) == ExecStatusType.PGRES_NONFATAL_ERROR)
				_error(res, sql, arguments);
			return res;
		}
	}
	
	/**
		Executes a SQL query (given as either a string or a CompiledTemple, either with or without arguments)
		and returns value first row, converted to type ResType.
		
		Example:
			ulong numOfUsers = Db.execSqlAndGet!ulong(`SELECT COUNT(*) FROM users`);
	 */
	static ResType execSqlAndGet(ResType=size_t, S, T...)(S query, T arguments) {
		PGresult* res = Db.execSql(query, arguments);
		return Row(res,0).get!ResType(0);
	}
	
	static bool isOk(PGresult* res) {
		auto status = PQresultStatus(res);
		return (status == ExecStatusType.PGRES_COMMAND_OK || status == ExecStatusType.PGRES_TUPLES_OK);
	}
	
	private static _error(PGresult* res, in string sql, in SqlArg[] arguments=[]) {
		// Get error's type (e.g. "PGRES_FATAL_ERROR") and full description
		immutable string status = (cast(char*) PQresStatus(PQresultStatus(res))).to!string;
		immutable string message = (cast(char*) PQresultErrorMessage(res)).to!string;
		
		// Construct full arguments list
		string argumentsString;
		foreach (i, argument; arguments)
			argumentsString ~= "\t$" ~ (i+1).to!string ~ ": " ~ argument.value.to!string;
		if (argumentsString.length > 0)
			argumentsString = "Arguments:\n" ~ argumentsString;
		
		// Throw it, so it can be logged and shown as a HTTP 500 error
		throw new Throwable(status ~ "\n" ~ message ~ "\n" ~ argumentsString);
	}
	
	/**
		A convenient interface for all the transaction functions here.
		Just pass some function here, and Db will try to execute it inside a transaction.
	 */
	static void transaction(void delegate() callback) {
		// Begin transaction/add savepoint
		if (++transactionLevel == 1)
			Db.execSql(`BEGIN`);
		else
			Db.execSql(`SAVEPOINT _ar_`~(++savepointsCounter).to!string);
		
		try {
			// Execute given code
			callback();
			
			// For first-level transaction, commit changes
			if (transactionLevel-- == 1)
				Db.execSql(`COMMIT`);
			
			// Return true.
			// It makes possible expressions like «Db.transaction({...}) && return».
		}
		catch (Exception e) {
			// Rollback to point where we began
			if (transactionLevel-- == 1)
				Db.execSql(`ROLLBACK`);
			else
				Db.execSql(`ROLLBACK TO _ar_`~savepointsCounter.to!string);
			
			// Throw the exception further
			throw e;
		}
	}
}