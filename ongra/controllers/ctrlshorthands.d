module ongra.controllers.ctrlshorthands;

import ongra;

void action_Main() {
	App.rt.indexable = true;
	
	if (App.ram.user_id)
		action_WhatsNew();
	else
		action_EditorsChoice();
}

void action_Static() {
	App.response.redirect(App.url!"Main");
}

void action_Ajax() {
	render!"zh"();
}