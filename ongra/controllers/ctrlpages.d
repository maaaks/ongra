module ongra.controllers.ctrlpages;

import ongra;

void action_Credits() {
	App.rt.title = say!"credits/title";
	render!"credits"();
}