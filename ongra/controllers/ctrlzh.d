module ongra.controllers.ctrlzh;

import curtain.activerecord;
import derelict.pq.pq;
import ongra;
import std.array;
import std.conv;
import std.datetime;
import std.typecons;
import vibe.data.json;

/**
	Show all the comments for the given Entry.
 */
void action_ShowComments() {
	string pubid = App.request.query.get("pubid");
	
	// Get the Entry's id
	ulong id = findEntryByPubid!long(pubid);
	
	// Now use standard query to load all the comments tree
	auto result = Db.execSql(renderSql!"Post", id);
	Entry post = Entry.fromTree!"children"(result, "entry.")[0];
	post.pf_commentsCount = PQntuples(result) - 1;
	
	// Mark all the comments read
	if (App.ram.user_id) {
		string[] entryIds;
		for (size_t i=0; i<PQntuples(result); i++)
			entryIds ~= Row(result, i).get!size_t("entry.id").to!string;
		Db.execSql(`UPDATE inbox SET unread=false WHERE subscriber_id=$1 AND entry_id IN(`~entryIds.join(",")~`)`,
			App.ram.user_id);
	}
	
	// Render
	render!"parts/allcomments"(tuple!("post")(post));
}


/**
	Add a reply for given Entry (either a post or a comment).
	
	It's one of the most boring method names here in CtrlZh.
 */
void action_Reply() {
	string rootPubid = App.request.form.get("root");
	string parentPubid = App.request.form.get("parent");
	string content = App.request.form.get("content");
	bool hamsterMode = App.request.form.get("hamster-mode", "off") != "off";
	
	// Check that all the required data is present
	if (content.length == 0)
		throw new Http!400(say!"errors/comment cannot be empty");
	
	// Find the root entry
	Entry root = findEntryByPubid(rootPubid);
	if (root is null)
		throw new Http!400(say!"errors/incorrect root pubid");
	
	// Find the entry to reply to
	ulong parentId = findEntryByPubid!ulong(parentPubid);
	if (parentId == 0)
		throw new Http!400(say!"errors/incorrect parent pubid");
	
	// Save the comment
	Entry comment = new Entry;
	comment.author_id = App.ram.user_id;
	comment.parent_id = parentId;
	comment.blog_id = hamsterMode ? App.ram.user.primaryBlog.id : comment.parent.blog_id; // TODO: choose blog
	comment.content = content;
	comment.type = hamsterMode ? Entry.Type.Post : Entry.Type.Comment;
	comment.status = Entry.Status.Published;
	comment.save();
	
	// Notify parent's subscribers about the new comment
	// (but don't notify the commenter itself if he is the parent's author, too)
	if (App.ram.user_id != comment.parent.author_id)
		Db.execSql(`INSERT INTO inbox(subscriber_id, entry_id) VALUES($1, $2)`, comment.parent.author_id, comment.id);
	
	// Render the new comment (so that JavaScript will be able to show it on the page)
	render!"parts/comment"(tuple!("post", "comment")(root, comment));
}


/**
	Add a user to your friends.
 */
void action_Subscribe() {
	string blog = App.request.form.get("blog");
	
	try {
		Db.execSql(`
			INSERT INTO subscriptions(subscriber_id, blog_id, access)
			VALUES($1, (SELECT id FROM blogs WHERE name=$2), 'Read')
		`, App.ram.user_id, blog);
		App.write("OK");
	}
	catch(Db.DbException e) {}
}


/**
	Remove a user from your friends.
 */
void action_Unsubscribe() {
	string blog = App.request.form.get("blog");
	
	Db.execSql(`
		DELETE FROM subscriptions
		WHERE subscriber_id=$1 AND blog_id=(SELECT id FROM blogs WHERE name=$2)
	`, App.ram.user_id, blog);
	App.write("OK");
}


void action_ApproveEntry() {
	immutable string entryPubid = App.request.form["pubid"];
	immutable bool showClubLink = App.request.form.get("showClubLink", "").length > 0;
	
	Entry entry = findEntryByPubid(entryPubid);
	entry.status = Entry.Status.Published;
	entry.save();
	
	Json json = Json.emptyObject();
	json["pubid"] = pubid(entry);
	json["textmeta"] = entry.htmlMeta(showClubLink);
	App.write(json);
}