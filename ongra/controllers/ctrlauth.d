module ongra.controllers.ctrlauth;

import curl = std.net.curl;
import curtain.activerecord;
import derelict.pq.pq;
import ongra;
import std.regex;
import std.string;
import std.typecons;
import vibe.data.json;
import vibe.http.client;
import vibe.textfilter.urlencode;

void action_Login() {
	// If a user is already logged in, just redirect him to a page
	if (App.ram.user_id != 0) {
		string targetUrl = App.request.query.get("go", App.url!"Main");
		App.response.redirect(targetUrl);
		return;
	}
	// If user is not authorized and login and password are entered, try to authorize user
	else if ("email" in App.request.form && "password" in App.request.form) {
		immutable string email = App.request.form["email"];
		immutable string hash = Auth_Classic.hash(App.request.form["password"]);
		
		try {
			App.ram.user_id = Db.execSqlAndGet!ulong(renderSql!"auth/Classic", email, hash);
			App.ram.save();
			
			string targetUrl = App.request.query.get("go", App.url!"Main");
			App.response.redirect(targetUrl);
			return;
		}
		catch(Row.OutOfRangeException e) {
			App.rt.title = say!"login/title";
			bool incorrect = true;
			render!"login"(tuple!("incorrect")(true));
		}
	}
	// If user is not authorized and has not sent login data, show the empty form
	else {
		App.rt.title = say!"login/title";
		render!"login";
	}
}


void action_Logout() {
	App.ram.nullify_user_id();
	App.ram.save();
	App.response.redirect(App.request.headers.get("Referer", "/"));
}


void action_Register() {
	// If user has already sent form, check it
	if ("ok" in App.request.form) {
		Db.transaction({
			// Check which fields are entered
			auto email    = App.request.form.get("email", null);
			auto password = App.request.form.get("password", null);
			auto nick     = App.request.form.get("nick", null);
			
			// Check that user entered all required fields
			if (!email.length || !password.length || !nick.length) {
				App.rt.title = say!"signup/title";
				render!"login"(tuple!
					("registerMode", "email", "password", "nick", "gotEmpty")
					( true,           email,   password,   nick,   true));
				return;
			}
			
			// Check that email is unique
			if (Db.execSqlAndGet!ulong(`SELECT COUNT(*) FROM auth_classic WHERE email=$1`, email) > 0) {
				App.rt.title = say!"signup/title";
				render!"login"(tuple!
					("registerMode", "email", "password", "nick", "notUnique")
					( true,           email,   password,   nick,   true));
				return;
			}
			
			// Check that password is long enough
			if (password.length < 8) {
				App.rt.title = say!"signup/title";
				render!"login"(tuple!
					("registerMode", "email", "password", "nick", "shortPassword")
					( true,           email,   password,   nick,   true));
				return;
			}
			
			// Register the user
			auto user = new User;
			user.nick = nick;
			user.save();
			auto auth = new Auth_Classic;
			auth.user = user;
			auth.email = email;
			auth.sha512 = Auth_Classic.hash(password);
			auth.activation_code = generateRandomString!64;
			auth.save();
			
			// Prepare activation email body
			string url = App.url!"EmailActivation"
				~ "?email=" ~ urlEncode(email)
				~ "&code=" ~ urlEncode(auth.activation_code);
			string mailBody = renderEmail!"activation"(tuple!("nick", "url")(nick, url));
			
			// Send email
			std.process.spawnProcess(["sendemail",
				"-f", Config.mail.SmtpUsername,
				"-t", auth.email,
				"-u", say!"email/account activation title",
				"-m", mailBody,
				"-s", Config.mail.SmtpHost~":"~Config.mail.SmtpPort.to!string,
				"-xu", Config.mail.SmtpUsername,
				"-xp", Config.mail.SmtpPassword,
				"-o", "message-charset=utf-8",
				"-o", "tls=yes",
			]);
			
			// Show information message
			App.msgBox(say!"signup/title", say!"signup/wait for activation email");
			return;
		});
		
		return;
	}
	
	// Or, just show the form for the first time
	App.rt.title = say!"signup/title";
	render!"login"(tuple!("registerMode")(true));
}


void action_EmailActivation() {
	immutable string email = App.request.query["email"];
	immutable string activationCode = App.request.query["code"];
	
	auto result = Db.execSql(
		`UPDATE auth_classic
		SET activation_code = NULL
		WHERE email=$1 AND activation_code=$2
		RETURNING user_id`,
		email, activationCode);
	
	if (PQntuples(result) > 0) {
		App.ram.user_id = Row(result,0).get!ulong(0);
		App.ram.save();
		App.msgBox(say!"activation/title", say!"activation/welcome");
	}
	else
		App.msgBox(say!"activation/title", say!"activation/incorrect code");
}


/**
	Callback URL for authorization using Yandex's OAuth.
 */
void action_OAuthYandex() {
	string code = App.request.query.get("code");
	auto info = _getInfoByYandexCode(code);
	
	// Find authorization method for given login
	Auth_Yandex auth = Auth_Yandex.find(`login=$1`, info.login);
	
	// If authorization method is found and is already active, just log the user in
	if (auth) {
		App.ram.user_id = auth.user_id;
		App.ram.save();
		App.redirect!"Main"();
	}
	// Else, if there is yet no user with given Yandex login, create it
	else {
		// Create the new user
		auto user = new User;
		user.nick = info.nick;
		user.save();
		
		// Create the authentification method for the new user
		auth = new Auth_Yandex;
		auth.user = user;
		auth.login = info.login;
		auth.active = true;
		auth.save();
		
		// Update session
		App.ram.user_id = user.id;
		App.ram.save();
		
		// Go to main page
		App.redirect!"Main"();
	}
}


alias Tuple!(string, "login", string, "nick") YandexInfo;
private YandexInfo _getInfoByYandexCode(in string code) {
	// If an empty code given, check what happened
	if (!code.length) {
		if (App.request.query.get("error") == "access_denied") {
			// User doesn't want to login with Yandex. Just go back to mainpage
			throw new Http!302(App.url!"Main");
		}
		else {
			// Some other error
			string error_description = App.request.query.get("error_description");
			throw new Http!500(say!"oauth/error title", error_description);
		}
	}
	
	string oauthResponse = cast(string) curl.post("https://oauth.yandex.ru/token",
		"grant_type=authorization_code"
		~"&code="~code
		~"&client_id="~Config.external.YandexClientId
		~"&client_secret="~Config.external.YandexPassword);
	
	Json oauthJson = parseJson(oauthResponse);
	string token = oauthJson["access_token"].get!string;
	
	// Now, when we have the OAuth token, get the user information from Yandex.Passport
	Json passportJson = requestHTTP("https://login.yandex.ru/info?oauth_token="~token).readJson();
	YandexInfo info;
	info.login = passportJson["login"].get!string;
	info.nick = passportJson["display_name"].get!string;
	
	// Make sure that user is a real @yandex.ru user, not a custom-domained one
	if (info.login.match(`@`))
		throw new Http!500(say!"oauth/error title", say!"oauth/yandex pdd");
	
	// If everything ok, return the information
	return info;
}