module ongra.controllers.ctrlspecials;

import ongra;
import std.conv;
import std.typecons;

void action_EditorsChoice() {
	immutable ulong page = App.request.query.get("page", "1").to!ulong;
	
	std.stdio.writeln(renderSql!"EditorsChoice");
	
	auto result = Db.execSql(renderSql!"EditorsChoice", page);
	auto count = Db.execSqlAndGet!ulong(renderSql!"count/CountEditorsChoice");
	
	App.rt.title = say!"feeds/best"~" — "~Config.meta.SiteName;
	render!"editorschoice"(tuple!
		("posts",               "paginator")
		(Entry.fromRows(result), new TwoWayPaginator(page, count)));
}

void action_WhatsNew() {
	if (!App.ram.user_id)
		App.redirect!"Main"();
	
	immutable ulong page = App.request.query.get("page", "1").to!ulong;
	
	auto result = Db.execSql(renderSql!"WhatsNew", App.ram.user_id, page);
	auto count = Db.execSqlAndGet!ulong(renderSql!"count/CountWhatsNew", App.ram.user_id);
	
	App.rt.title = say!"feeds/whatsnew"~" — "~Config.meta.SiteName;
	render!"whatsnew"(tuple!
		("posts",                "paginator")
		( Entry.fromRows(result), new TwoWayPaginator(page, count)));
}

void action_AllPosts() {
	immutable ulong page = App.request.query.get("page", "1").to!ulong;
	
	auto result = Db.execSql(renderSql!"AllPosts", page);
	auto count = Db.execSqlAndGet!ulong(renderSql!"count/CountAllPosts");
	
	App.rt.title = say!"feeds/all"~" — "~Config.meta.SiteName;
	render!"whatsnew"(tuple!
		("posts",                "paginator")
		( Entry.fromRows(result), new TwoWayPaginator(page, count)));
}