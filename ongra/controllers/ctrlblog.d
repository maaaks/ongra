module ongra.controllers.ctrlblog;

import curtain.activerecord;
import derelict.pq.pq;
import ongra;
import std.array;
import std.conv;
import std.typecons;


/**
	If given login corresponds to a passive blog account, calls onPassiveFound().
	If it corresponds to a blog that has been activated, calls onActiveFound().
	Otherwise, calls onNobodyFound().
	
	This method DOES NOT check whether a non-yaru namesake with same name exists.
 */
private void findActiveOrPassiveBlog(
	in string login,
	void delegate(Blog) onActiveFound,
	void delegate(Blog) onPassiveFound,
	void delegate() onNobodyFound
) {
	if (Blog passiveBlog = Blog.find(`name=$1`, "ya:"~login))
		onPassiveFound(passiveBlog);
	else if (Blog activeBlog = Blog.find(`name=$1 AND fromya='true'`, login))
		onActiveFound(activeBlog);
	else
		onNobodyFound();
}


alias Tuple!(Entry[],"entries", TwoWayPaginator,"paginator") EntriesAndPaginator;
EntriesAndPaginator _loadEntriesAndPaginator(in string blogName, ulong page, in string tag=null) {
	// Prepare runtime params
	Db.SqlArg[] sqlParams;
	sqlParams ~= Db.createSqlArg(blogName);
	sqlParams ~= Db.createSqlArg(App.ram.user_id);
	sqlParams ~= Db.createSqlArg(page);
	if (tag !is null) {
		sqlParams ~= Db.createSqlArg(tag);
		sqlParams ~= Db.createSqlArg(tag.replace("_", " "));
	}
	
	// Load entries
	string query1 = renderSql!"Blog"(tuple!("filterByTag")(tag !is null));
	PGresult* result = Db.execSql(query1, sqlParams);
	
	// Get count of entries
	string query2 = renderSql!"Blog"(tuple!("onlyCount", "filterByTag")(true, tag !is null));
	ulong count = Db.execSqlAndGet!ulong(query2, sqlParams);
	
	// Return
	return EntriesAndPaginator(
		Entry.fromRows(result),
		new TwoWayPaginator(page, count)
	);
}


/**
	Aux method for rendering a blog.
 */
private void renderBlog(Blog blog) {
	immutable string pageAsString = App.request.query.get("page", null);
	immutable ulong page = pageAsString ? pageAsString.to!ulong : 1;
	if (pageAsString is null)
		App.rt.indexable = true;
	
	App.rt.title = blog.title;
	
	auto eap = _loadEntriesAndPaginator(blog.name, page);
	render!"blog"(tuple!
		("blog", "entries",   "paginator")
		( blog,   eap.entries, eap.paginator));
}


////////////////////////////////////////////////////////////////////////////////



/**
	Get a blog by the name and show a page with its posts.
	Shows 404 when blog is not found.
 */
void action_Blog() {
	Blog blog = Blog.find(`name=$1`, App.request.params["name"]);
	if (blog && !blog.deleted)
		renderBlog(blog);
	else
		throw new Http!404;
}


/**
	Show a message about passive blog imported from Ya.ru.
	If the blog was activated, redirect to its blog.
 */
void action_PassiveBlog() {
	immutable string name = App.request.params["name"];
	
	findActiveOrPassiveBlog(name,
		(Blog activeBlog){ throw new Http!301(App.router.makeLink(activeBlog)); },
		(Blog passiveBlog){ throw new Http!404; },
		(){ throw new Http!404; }
	);
}


/**
	Same as action_Blog(), but shows only posts with given tag.
 */
void action_BlogTag() {
	immutable string name = App.request.params["name"];
	immutable string tag = App.request.params["tag"];
	immutable ulong page = App.request.query.get("page", "1").to!ulong;
	
	auto eap = _loadEntriesAndPaginator(name, page, tag);
	render!"blog"(tuple!
		("blog",                     "entries",   "paginator")
		( Blog.find(`name=$1`, name), eap.entries, eap.paginator));
}


/**
	Show a single post from a blog or a club.
 */
void action_Post(string name=null, ulong localId=0, Entry.Space space=Entry.Space.Default) {
	if (!name)    name    = App.request.params["name"];
	if (!localId) localId = App.request.params["local_id"].to!ulong;
	
	// Get the entry id
	ulong id = findEntryByPubid!ulong(name, space, localId);
	if (id == -1)
		throw new Http!404;
	
	// Now load the full Entry object with tree of children
	auto result = Db.execSql(renderSql!"Post", id);
	Entry[] entries = Entry.fromTree!"children"(result, "entry.");
	if (!entries.length)
		throw new Http!404;
	Entry entry = entries[0];
	entry.pf_commentsCount = PQntuples(result) - 1;
	
	// Check access of current user to the loaded Entry
	if (!Access.canSee(entry))
		throw new Http!403;
	
	// Mark all the comments read
	if (App.ram.user_id) {
		string[] entryIds;
		for (size_t i=0; i<PQntuples(result); i++)
			entryIds ~= Row(result, i).get!size_t("entry.id").to!string;
		Db.execSql(`UPDATE inbox SET unread=false WHERE subscriber_id=$1 AND entry_id IN(`~entryIds.join(",")~`)`,
			App.ram.user_id);
	}
	
	// Make this page indexable
	App.rt.indexable = true;
	
	// Render the page
	App.rt.title = entry.title~" - "~entry.blog.title;
	render!"post"(tuple!("post")(entry));
}

void action_PostDraft() {
	action_Post(null, 0, Entry.Space.Draft);
}


/**
	Show a post of a passive blog imported from Ya.ru.
	If the blog was activated, redirect to the new URL of this post.
 */
void action_PassiveBlogPost() {
	immutable string name = App.request.params["name"];
	immutable string localId = App.request.params["local_id"];
	
	findActiveOrPassiveBlog(name,
		(Blog activeBlog){
			immutable string url = App.url!"Post"([
				"name":       activeBlog.name,
				"localpubid": "ya"~localId,
			]);
			throw new Http!301(url);
		},
		(Blog passiveBlog){
			action_Post("ya:"~name, localId.to!ulong);
		},
		(){ throw new Http!404; }
	);
}