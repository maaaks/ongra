module ongra.controllers.ctrlcabinet;

import curtain.activerecord;
import derelict.pq.pq;
import ongra;
import std.algorithm;
import std.array;
import std.conv;
import std.format;
import std.regex;
import std.typecons;
import vibe.inet.webform;

void action_Settings() {
	// TODO: settings
}

/**
	Show unread replies for current user.
 */
void action_Inbox() {
	if (!App.ram.user_id)
		App.redirect!"Main"();
	
	immutable ulong page = App.request.query.get("page", "1").to!ulong;
	
	auto result = Db.execSql(renderSql!"Inbox", App.ram.user_id, page);
	auto count = Db.execSqlAndGet!ulong(renderSql!"count/CountInbox", App.ram.user_id);
	
	alias Tuple!(User, "user", Entry, "entry", bool, "unread") ReplyInfo;
	ReplyInfo[] replies;
	foreach (i; 0..PQntuples(result)) {
		Row row = Row(result, i);
		
		ReplyInfo info;
		info.user = User.fromRow(row, "user.");
		info.entry = Entry.fromRow(row, "entry.");
		info.unread = (row.get!string("unread") == "t");
		replies ~= info;
	}
	
	// Pass data to Temple and render
	App.rt.title = say!"inbox/title"~" - "~Config.meta.SiteName;
	render!"inbox"(tuple!
		("replies", "paginator")
		( replies,   new TwoWayPaginator(page, count, Config.display.RepliesPageSize)));
}


void action_NewPost() {
	auto post = new Entry;
	post.blog = Blog.find(`name=$1`, App.request.params["name"]);
	_writeOrEditPost(post);
}

void action_EditPost() {
	auto post = findEntryByPubid(App.request.params["name"], Entry.Space.Default, App.request.params["local_id"].to!ulong);
	_writeOrEditPost(post);
}

void action_EditPostDraft() {
	auto post = findEntryByPubid(App.request.params["name"], Entry.Space.Draft, App.request.params["local_id"].to!ulong);
	_writeOrEditPost(post);
}


private void _writeOrEditPost(Entry entry) {
	// For authorized users only
	if (!App.ram.user_id)
		throw new Http!500(say!"access/error title", say!"access/authorized users only");
	
	// If entry is passed here, use it. Else, create a new one
	if (!entry) entry = new Entry;
	
	// For new entry, choose blog to place it to
	if (!entry.isSaved())
		entry.blog = Blog.find(`name=$1`, App.request.params["name"]);
	
	immutable string action = App.request.form.get("action");
	if (action == "compare-with-html") {
		// Preview changes
		App.rt.title = say!"write/preview";
		App.rt.fullWidth = true;
		render!"preview"(tuple!
			("post", "newTitle",                    "newContent")
			( entry,  App.request.form.get("title"), App.request.form.get("title")));
		return;
	}
	else if (action == "save") {
		// Fill the Entry with the data from POST
		entry.title   = App.request.form.get("title");
		entry.content = App.request.form.get("content");
		entry.tags    = App.request.form.getAll("tags[]");
		
		if (!Access.canWrite(entry.blog)) {
			App.msgBox(say!"access/error title", say!"access/you are just too bad");
		}
		
		if (entry.content.length != 0) {
			// Set author if not set
			if (entry.author_id == 0)
				entry.author_id = App.ram.user_id;
			
			// Choose entry's status
			if (!entry.id)
				entry.status = Access.canWriteImmediately(entry.blog)
					? Entry.Status.Published
					: Entry.Status.OnModeration;
			
			// Save entry to database
			entry.syntax = Entry.Syntax.Liara;
			entry.save();
			
			// Redirect to the page of the new entry
			App.response.redirect(App.router.makeLink(entry));
			return;
		}
	}
	
	// Render
	App.rt.title = say!"write/write";
	render!"write"(tuple!("post")(entry));
}


void action_NewBlog() {
	action_BlogSettings!"NewBlog"();
}


void action_BlogSettings(string mode="EditBlog")() {
	static if (mode == "NewBlog") {
		Blog blog = new Blog;
		blog.name = App.request.form.get("name", "");
	} else {
		Blog blog = Blog.find(`name=$1`, App.request.params["name"]);
	}
	
	// Check permissions
	if (!App.ram.user_id)
		throw new Http!403;
	if (mode=="EditBlog" && !Access.canChangeSettings(blog))
		throw new Http!403;
	
	blog.title       = App.request.form.get("title",       blog.title);
	blog.description = App.request.form.get("description", blog.description);
	blog.type        = App.request.form.get("type",        blog.type.to!string).to!(Blog.Type);
	bool submitted = (App.request.form.get("ok", "").length > 0);
	string error;
	
	if (submitted) {
		error = _validateBlogProperties!mode(blog);
		if (!error) {
			blog.userpic = Upload.create(App.request.files.get("avatar"), App.ram.user);
			blog.owner = App.ram.user;
			blog.save();
			
			static if (mode == "NewBlog") {
				// If this is the user's first blog (excluding Ya.Ru blogs), make it primary
				ulong numOfBlogs = Db.execSqlAndGet!ulong(`SELECT COUNT(*) FROM blogs WHERE owner_id=$1 AND fromya=false`,
					App.ram.user_id);
				if (numOfBlogs == 1) {
					Db.transaction({
						Db.execSql(`UPDATE subscriptions SET access='Admin' WHERE subscriber_id=$1 AND access='Primary'`,
							App.ram.user_id);
						Db.execSql(`INSERT INTO subscriptions(subscriber_id, blog_id, access) VALUES($1, $2, $3)`,
							App.ram.user_id, blog.id, Blog.Access.Primary);
					});
				}
				// Else, just make user the blog's admin
				else {
					Db.execSql(`INSERT INTO subscriptions(subscriber_id, blog_id, access) VALUES($1, $2, $3)`,
						App.ram.user_id, blog.id, Blog.Access.Admin);
				}
			}
			
			// Finally, redirect to the new blog
			throw new Http!302(App.router.makeLink(blog));
		}
	}
	
	// Get parts of blog URLs, e.g. "[name].ongra.net/" becomes "" and "ongra.net".
	string[2] urlParts = Config.routes.Blog.templateString.replaceAll(ctRegex!`/$`, "").split("[name]");
	
	// Show the form, with or without error message
	App.rt.title = (mode == "NewBlog") ? say!"blogs/create" : say!"blogs/properties";
	if (error) App.rt.messages ~= error;
	render!"newblog"(tuple!
		("isNewBlog",      "blog", "urlPrefix", "urlPostfix")
		((mode=="NewBlog"), blog,   urlParts[0], urlParts[1]));
}


/**
	Aux function for action_BlogSettings(). Returns error text or null if no errors found.
 */
string _validateBlogProperties(string mode="EditBlog")(Blog blog) {
	// Check that fields exist and are not empty
	if (!blog.title.length)
		return say!"errors/blog title required";
	if (!blog.name.length)
		return (blog.type==Blog.Type.Premoderated || blog.type==Blog.Type.Public)
			? say!"errors/club url required"
			: say!"errors/blog url required";
	
	// Check that entered name is allowed
	if (["about", "api", "auth", "clubs", "dev", "help", "mail", "www"].canFind(blog.name))
		return say!"errors/club url blacklisted".format(blog.name);
	
	// Check that links to this blog won't conflict with links to somebody's local posts
	if (blog.name.match(ctRegex!`^(ya|draft|)\d+$`))
		return say!"errors/numeric club names are forbidden";
	
	static if (mode == "NewBlog") {
		// Check that entered name is not too long or too short
		if (!blog.name.match(ctRegex!`^[A-z0-9-_]{3,50}$`))
			return say!"errors/club name length limits";
		
		// Check availiability of entered name
		Blog existingBlog = Blog.find(`name=$1`, blog.name);
		if (existingBlog) {
			if (existingBlog.isClub())
				return say!"errors/club name taken".format(existingBlog.htmlLink(true));
			else
				return say!"errors/blog name taken".format(existingBlog.htmlLink(true));
		}
	}
	
	// Check that description is not to long or too short
	if (blog.description.length > 1000)
		return say!"errors/club name too long".format(1000);
	
	// If we are still here, it means that the input is perfect. Return no error.
	return null;
}


void action_DeleteBlog() {
	immutable string name = App.request.params["name"];
	immutable string ok = App.request.form.get("ok", null);
	
	auto blog = Blog.find(`name=$1`, name);
	if (!Access.canDelete(blog))
		throw new Http!403;
	
	if (ok) {
		Db.execSql(`UPDATE blogs SET deleted=true WHERE name=$1`, name);
		throw new Http!302(App.url!"Main");
	}
	
	App.rt.title = say!"blogs/delete title";
	render!"deleteblog"(tuple!("blog")(blog));
}


void action_BlogsByUser() {
	string name = App.request.params["name"];
	bool me = (name == App.ram.user.primaryBlog.name);
	bool writingMode = (me && App.request.query.get("write"));
	
	// Load data with a single query to database
	auto res = Db.execSql(renderSql!"BlogsByUser", name, Blog.Access.Read);
	
	// Get the primary blog. If not this was used in URL, throw «Not Found»
	Blog primaryblog = Blog.fromRow(res, 0, "primaryblog.");
	if (primaryblog.name != name)
		throw new Http!404;
	
	// Get arrays of all blogs, divided into three categories
	Blog[] blogsOwned, blogsModerated, blogsRead;
	foreach (i; 0..PQntuples(res)) {
		Row row = Row(res, i);
		final switch (row.get!(Blog.Access)("access")) {
			case Blog.Access.None:
				break;
				
			case Blog.Access.Invited:
				break; // TODO: show invitations here
				
			case Blog.Access.Read:
				blogsRead ~= Blog.fromRow(row);
				break;
				
			case Blog.Access.ReadWrite:
			case Blog.Access.Moderator:
				blogsModerated ~= Blog.fromRow(row);
				break;
				
			case Blog.Access.Admin:
			case Blog.Access.Primary:
				blogsOwned ~= Blog.fromRow(row);
				break;
		}
	}
	
	App.rt.title     = writingMode
		? say!"blogs/write to blog"
		: (me
			? say!"blogs/me"
			: primaryblog.owner.nick);
	
	render!"blogslist"(tuple!(
		"title",
		"titleOwned",
		"titleModerated",
		"titleRead",
		"blogsOwned",
		"blogsModerated",
		"blogsRead",
		"showBtnNewBlog",
		"writingMode"
	)(
		App.rt.title,
		writingMode ? "" : (me ? say!"blogs/i write"    : say!"blogs/he writes"),
		writingMode ? "" : (me ? say!"blogs/i moderate" : say!"blogs/he moderates"),
		writingMode ? "" : (me ? say!"blogs/i read"     : say!"blogs/he reads"),
		blogsOwned,
		blogsModerated,
		blogsRead,
		me,
		writingMode
	));
}