module ongra.language;

import ongra;


mixin Phrase!("access/authorized users only");
mixin Phrase!("access/error title");
mixin Phrase!("access/you are just too bad");

mixin Phrase!("activation/incorrect code");
mixin Phrase!("activation/title");
mixin Phrase!("activation/welcome");

mixin Phrase!("blogs/create");
mixin Phrase!("blogs/properties");
mixin Phrase!("blogs/delete title");
mixin Phrase!("blogs/me");
mixin Phrase!("blogs/write to blog");
mixin Phrase!("blogs/write to this blog");
mixin Phrase!("blogs/i write");
mixin Phrase!("blogs/i moderate");
mixin Phrase!("blogs/i read");
mixin Phrase!("blogs/he writes");
mixin Phrase!("blogs/he moderates");
mixin Phrase!("blogs/he reads");
mixin Phrase!("blogs/i have n followers");
mixin Phrase!("blogs/n readers");
mixin Phrase!("blogs/n members");
mixin Phrase!("blogs/blog settings");
mixin Phrase!("blogs/new blog");
mixin Phrase!("blogs/confirm deletion p1");
mixin Phrase!("blogs/confirm deletion p2");
mixin Phrase!("blogs/confirm deletion p3");
mixin Phrase!("blogs/yes delete blog");
mixin Phrase!("blogs/delete blog");

mixin Phrase!("comment/placeholder");
mixin Phrase!("comment/hamster mode");
mixin Phrase!("comment/ok");
mixin Phrase!("comment/replied in own blog");
mixin Phrase!("comment/reply to reply");

mixin Phrase!("credits/title");
mixin Phrase!("credits/body");

mixin Phrase!("email/account activation title");

mixin Phrase!("errors/blog title required");
mixin Phrase!("errors/blog url required");
mixin Phrase!("errors/club url required");
mixin Phrase!("errors/club url blacklisted");
mixin Phrase!("errors/numeric club names are forbidden");
mixin Phrase!("errors/club name length limits");
mixin Phrase!("errors/blog name taken");
mixin Phrase!("errors/club name taken");
mixin Phrase!("errors/club name too long");
mixin Phrase!("errors/comment cannot be empty");
mixin Phrase!("errors/incorrect root pubid");
mixin Phrase!("errors/incorrect parent pubid");
mixin Phrase!("errors/http error title");

mixin Phrase!("feeds/best");
mixin Phrase!("feeds/best/in footer");
mixin Phrase!("feeds/whatsnew");
mixin Phrase!("feeds/whatsnew/description");
mixin Phrase!("feeds/all");

mixin Phrase!("inbox/title");
mixin Phrase!("inbox/n replies");
mixin Phrase!("inbox/mark all read");
mixin Phrase!("inbox/topic");
mixin Phrase!("inbox/author");
mixin Phrase!("inbox/comment");

mixin Phrase!("login/title");
mixin Phrase!("login/email");
mixin Phrase!("login/email placeholder");
mixin Phrase!("login/password");
mixin Phrase!("login/ok");

mixin Phrase!("menu/login");
mixin Phrase!("menu/signup");
mixin Phrase!("menu/write");
mixin Phrase!("menu/write to blog");
mixin Phrase!("menu/my blogs");
mixin Phrase!("menu/logout");
mixin Phrase!("menu/new blog");

mixin Phrase!("newblog/url");
mixin Phrase!("newblog/url placeholder");
mixin Phrase!("newblog/title");
mixin Phrase!("newblog/title placeholder");
mixin Phrase!("newblog/description");
mixin Phrase!("newblog/description placeholder");
mixin Phrase!("newblog/userpic");
mixin Phrase!("newblog/type");
mixin Phrase!("newblog/type/personal");
mixin Phrase!("newblog/type/personal/description");
mixin Phrase!("newblog/type/premoderated");
mixin Phrase!("newblog/type/premoderated/description");
mixin Phrase!("newblog/type/public");
mixin Phrase!("newblog/type/public/description");
mixin Phrase!("newblog/ok create");
mixin Phrase!("newblog/ok save");

mixin Phrase!("oauth/error title");
mixin Phrase!("oauth/yandex pdd");

mixin Phrase!("paginator/later");
mixin Phrase!("paginator/earlier");

mixin Phrase!("signup/title");
mixin Phrase!("signup/wait for activation email");
mixin Phrase!("signup/welcome to signup form");
mixin Phrase!("signup/email");
mixin Phrase!("signup/email placeholder");
mixin Phrase!("signup/password");
mixin Phrase!("signup/password placeholder");
mixin Phrase!("signup/nick");
mixin Phrase!("signup/nick placeholder");
mixin Phrase!("signup/ok");
mixin Phrase!("signup/you can use yandex");
mixin Phrase!("signup/all fields are required");
mixin Phrase!("signup/incorrect login or password");
mixin Phrase!("signup/email is taken");
mixin Phrase!("signup/password is too short");

mixin Phrase!("write/preview");
mixin Phrase!("write/write");
mixin Phrase!("write/title placeholder");
mixin Phrase!("write/body placeholder");
mixin Phrase!("write/add tag");
mixin Phrase!("write/ok");