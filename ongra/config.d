module ongra.config;

import curtain.router;
import inilike;
import std.conv;
import vibe.mail.smtp;
import vibe.stream.ssl;


class Config
{
	static private Config instance;
	
	static struct struct_meta {
		string SiteName;
	}
	
	static struct struct_display {
		string DefaultLanguage;
		ushort PageSize;
		ushort RepliesPageSize;
	}
	
	static struct struct_server {
		string Host;
		string Port;
		string Scheme;
		string StaticDir;
		string LanguagesDir;
		string ErrorLog;
		string Session;
		string DefaultUserpicsDir;
	}
	
	static struct struct_mail {
		SMTPAuthType          SmtpAuthType;
		SMTPConnectionType    SmtpConnectionType;
		string                SmtpHost;
		string                SmtpPassword;
		ushort                SmtpPort;
		SSLPeerValidationMode SmtpSslValidationMode;
		string                SmtpUsername;
	}
	
	static struct struct_domains {
		string Main;
		string Static;
		string Ajax;
		string Cookies;
	}
	
	static struct struct_routes {
		// Roots
		Route Main;
		Route Static;
		Route Ajax;
		
		// Pages on the root domain
		Route Credits;
		Route EditorsChoice;
		Route AllPosts;
		
		// Pages on "my" domain
		Route Inbox;
		Route Settings;
		Route NewBlog;
		
		// Authentification
		Route Login;
		Route Logout;
		Route Register;
		Route OAuthYandex;
		Route EmailActivation;
		
		// Normal blogs
		Route Blog;
		Route Post;
		Route PostDraft;
		Route NewPost;
		Route EditPost;
		Route EditPostDraft;
		Route BlogTag;
		Route BlogsByUser;
		Route BlogSettings;
		Route DeleteBlog;
		
		// Imported blogs
		Route PassiveBlog;
		Route PassiveBlogPost;
		
		// Ajax requests
		Route Subscribe;
		Route Unsubscribe;
		Route Reply;
		Route ShowComments;
		Route ApproveEntry;
	}
	
	static struct struct_db {
		string Host;
		string Username;
		string Password;
		string DbName;
	}
	
	static struct struct_external {
		string JamendoClientId;
		string YandexClientId;
		string YandexPassword;
	}
	
	static struct_display  display;
	static struct_server   server;
	static struct_mail     mail;
	static struct_meta     meta;
	static struct_domains  domains;
	static struct_routes   routes;
	static struct_db       db;
	static struct_external external;
	
	static void load(in string path) {
		instance = new Config(path);
	}
	
	private this(in string path) {
		IniLikeFile ini = new IniLikeFile(path);
		
		// Now, initialize real parameters from the dictionary
		foreach (section; __traits(allMembers, Config)) {
			static if (section.length > 7 && section[0..7] == "struct_") {
				foreach (param; __traits(allMembers, mixin("Config."~section))) {
					alias typeof(__traits(getMember, mixin("Config."~section), param)) Type;
					
					static if (is(Type == Route)) {
						mixin(section[7..$]~"."~param) = new Route(ini.group(section[7..$]).value(param, ""));
					} else static if (is(Type == string)) {
						mixin(section[7..$]~"."~param) = ini.group(section[7..$]).value(param, "");
					} else {
						mixin(section[7..$]~"."~param) = ini.group(section[7..$]).value(param, "").to!(Type);
					}
				}
			}
		}
	}
}