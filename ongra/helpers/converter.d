module ongra.helpers.converter;

import kxml.xml;
import std.array;
import std.regex;
import std.stdio;

string htmlToLiara(in string input) {
	auto html = XmlDocument(input, true);
	
	string output;
	foreach (XmlNode node; html.getChildren()) 
		output ~= nodeToLiara(node);
	output = output.strip();
	
	//writeln(output);
	return output;
}


string nodeToLiara(XmlNode node) {
	if (node.isCData())
		return node.getCData();
	
	switch (node.getName) {
		default:
			return "{{"~node.getName~"}} ";
			
		case "br":
			return "\n";
			
		case "b":
		case "strong":
			// This may be a link to Ya.Ru user:
			// <b class="b-user"><a class="b-user__link" href="http://greatperson.ya.ru/">
			// 		<b class="b-user__first-letter">g</b>reatperson</a>
			// </b>
			if (node.getAttribute("class") == "b-user"
			&& node.getChildren().length == 1
			&& !node.getChildren()[0].isCData()
			&& node.getChildren()[0].getName() == "a"
			&& node.getChildren()[0].getAttribute("class") == "b-user__link"
			&& node.getChildren()[0].getChildren().length == 2
			&& !node.getChildren()[0].getChildren()[0].isCData()
			&& node.getChildren()[0].getChildren()[0].getName() == "b"
			&& node.getChildren()[0].getChildren()[1].isCData()
			) {
				string href = node.getChildren()[0].getAttribute("href");
				auto m = href.match(ctRegex!`^http://(.+)\.ya\.ru/`);
				
				// If the link does not lead to a user, then it's a usual bold text
				if (!m) return "**"~nodesToLiara(node.getChildren())~"**";
				
				string login = m.captures[1];
				string text = node.getChildren()[0].getChildren()[0].getCData()
					~ node.getChildren()[0].getChildren()[1].getCData();
				
				if (text == login)
					return "((ya/"~login~"))";
				else
					return "((ya/"~login~" "~text~"))";
			}
			// Or, this may be a simple bold text
			else
				return "**"~nodesToLiara(node.getChildren())~"**";
			
		case "i":
		case "em":
			// This may be a smile:
			// <i class="b-smile &#10; b-smile-default js-smile-plain">
			// <i>:)</i>
			// <img class=" b-smile_emotion_1" src="//yandex.st/lego/_/La6qi18Z8LwgnZdsAr1qy1GwCwo.gif" title=":)" alt=":)" />
			// </i>
			if (node.getAttribute("class").match(ctRegex!`^b-smile `)
			&& node.getChildren().length == 2
			&& !node.getChildren()[0].isCData()
			&& node.getChildren()[0].getName() == "i"
			) {
				return "["~node.getChildren()[0].getCData()~"]"; // "[:-)]", "[=)]", "[:-(]", etc
			}
			// Or, this may be just an italic text
			else
				return "__"~nodesToLiara(node.getChildren())~"__";
		
		case "a":
			// This may be an image link
			auto children = node.getChildren();
			if (children.length == 1 && !children[0].isCData() && children[0].getName()=="img") {
				auto img = children[0];
				
				// Image's parameters
				string params;
				if (img.getAttribute("width") && img.getAttribute("height")) {
					if (!img.getAttribute("align") && img.getAttribute("width").to!size_t >= 800)
						params ~= " -wide";
					else
						params ~= " -size "~img.getAttribute("width")~"x"~img.getAttribute("height");
				}
				if (img.getAttribute("align") == "left") {
					params ~= " -left";
				}
				if (img.getAttribute("align") == "right") {
					params ~= " -right";
				}
				
				// This may be an image from Yandex.Fotki
				auto m1 = node.getAttribute("href").match(ctRegex!`^http://fotki.yandex.ru/users/([^/]+)/view/(\d+)$`);
				auto m2 = img.getAttribute("src").match(ctRegex!`^http://img-fotki.yandex.ru/get/`);
				if (m1 && m2)
					return "&yafotki " ~ m1.captures[1]~"/"~m1.captures[2] ~ params ~ "\n";
				
				// This may be some other external image
				
			}
			// Or, ot may be a simple link 
			else {
				immutable string href = node.getAttribute("href");
				immutable string text = nodesToLiara(node.getChildren());
				if (href == text)
					return "(("~href~"))";
				else
					return "(("~href~" "~text~"))";
			}
			
			return "";
			
		case "span":
			// This may be a cut: <span...><b>( <a...>29.VI.50 г.</a> )</b></span>
			if (node.getAttribute("class") == "cut-text") {
				immutable string label = node.parseXPath("/b/a")[0].getCData();
				return (label == "Читать дальше") ? "(((CUT)))" : "(((CUT:"~label~")))";
			}
			// This may be a link to user:
			// <span class="b-yauser"><a href="http://utochka-no4.ya.ru/"><b>Я</b>.Уточка (№4)</a></span>
			else if (node.getAttribute("class") == "b-yauser"
			&& node.getChildren().length == 1
			&& !node.getChildren()[0].isCData()
			&& node.getChildren()[0].getName() == "a"
			&& node.getChildren()[0].getChildren().length == 2
			&& !node.getChildren()[0].getChildren()[0].isCData()
			&& node.getChildren()[0].getChildren()[0].getName() == "b"
			&& node.getChildren()[0].getChildren()[1].isCData()
			) {
				string href = node.getChildren()[0].getAttribute("href");
				auto m = href.match(ctRegex!`^http://(.+)\.ya\.ru/`);
				
				// If the link does not lead to a user, then it's a usual useless span
				if (!m) return nodesToLiara(node.getChildren());
				
				string login = m.captures[1];
				string text = node.getChildren()[0].getChildren()[0].getCData()
					~ node.getChildren()[0].getChildren()[1].getCData();
				
				if (text == login)
					return "((ya/"~login~"))";
				else
					return "((ya/"~login~" "~text~"))";
			}
			// This may be an undercut
			else if (node.getAttribute("class") == "cut-content i-hidden") {
				return nodesToLiara(node.getChildren()) ~ "(((/CUT)))";
			}
			// This may be a fucking simple span
			else
				return nodesToLiara(node.getChildren());
			
		case "h1":
		case "h2":
			return "==== " ~ nodesToLiara(node.getChildren()) ~ "\n\n";
		case "h3":
		case "h4":
			return "=== " ~ nodesToLiara(node.getChildren()) ~ "\n\n";
		case "h5":
		case "h6":
			return "== " ~ nodesToLiara(node.getChildren()) ~ "\n\n";
			
		case "p":
		case "div":
			return nodesToLiara(node.getChildren()) ~ "\n\n";
			
		case "blockquote":
			return "> " ~ nodesToLiara(node.getChildren()).replace("\n", "\n> ") ~ "\n\n";
			
		case "iframe":
			// This may be an embedded Yandex.Video
			if (auto m = node.getAttribute("src").match(ctRegex!`^http://video.yandex.ru/iframe/(.+)/$`)) {
				immutable string width = node.getAttribute("width");
				immutable string height = node.getAttribute("height");
				return "&yavideo " ~ m.captures[1] ~ " -size="~width~"x"~height ~ "\n";
			}
			
			return "";
			
		case "object":
			// This may be an embedded Yandex.Music
			if (node.getChildren().length==3 && node.getChildren()[0].getName()=="param") {
				if (auto m = node.getChildren()[0].getAttribute("value").match(ctRegex!`^http://music.yandex.ru/embed/(\d+)/track.swf`)) {
					return "&yamusic " ~ m.captures[1] ~ "\n";
				}
			}
			
			return "";
			
		case "hr":
			return "----\n\n";
			
		case "wbr":
			return "";
	}
}

string nodesToLiara(XmlNode[] nodes) {
	string output;
	foreach (node; nodes)
		output ~= nodeToLiara(node);
	return output;
}