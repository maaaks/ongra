module ongra.helpers.pubid;

import curtain.activerecord;
import derelict.pq.pq;
import ongra;
import std.conv;
import std.string;
import std.regex;
import std.traits;
import std.typecons;

/**
	Returns public id («pubid») of given Entry.
	Pubid looks like «john/123» for posts or «john/123/4» for comments.
	First section is login of the rootParent's author, second section is rootParent's id,
	and third section is comment's id when the entry is a comment.
	
	See also: Entry.anchor().
 */
string pubid(Entry entry) {
	if (entry.type == Entry.Type.Post)
		return entry.blog.name ~ "/" ~ localPubid(entry); // john/123
	else
		return pubid(entry.rootParent()) ~ "/" ~ entry.seqnumber().to!string;  // john/123/4
}

string localPubid(Entry entry) {
	return _stringifyId(entry.space, entry.local_id);
}


/**
	Loads Entry by its pubid, like these:
	 — «john/123»: post #123 in John's blog
	 — «john/123/4»: somebody's comment on post #123 in John's blog
	
	Second part of pubid can be one of three types with independent number spaces:
	 — «123»: normal post on Ongra
	 — «draft123»: drafts or unpublished post
	 — «ya123»: post from YaRu
	
	If called with T=Entry, it returns an Entry object,
	if called with T=long, it returns only its id.
 */
T findEntryByPubid(T=Entry)(in string pubid) {
	return _byPubid_impl!T((out string query, out Db.SqlArg[] arguments, bool idOnly)
	{
		auto m = pubid.match(ctRegex!`([^/]+)/((?:ya)?\d+)(?:/(\d+))?`);
		if (m) {
			immutable string capName     = m.captures[1];
			immutable auto destringified = _destringifyId(m.captures[2]);
			immutable string capReplyId  = m.captures[3];
			
			if (capReplyId.length == 0) {
				// This is a blog entry
				query = renderSql!"bypubid/by-Name-Status-Id"(tuple!("idOnly")(idOnly));
				arguments ~= Db.createSqlArg(capName);
				arguments ~= Db.createSqlArg(destringified.space);
				arguments ~= Db.createSqlArg(destringified.id);
			} else {
				// This is a blog entry's comment
				query = renderSql!"bypubid/by-Name-Status-Id-ReplyId"(tuple!("idOnly")(idOnly));
				arguments ~= Db.createSqlArg(capName);
				arguments ~= Db.createSqlArg(destringified.space);
				arguments ~= Db.createSqlArg(destringified.id);
				arguments ~= Db.createSqlArg(capReplyId);
			}
		}
		else
			throw new Exception("Invalid pubid: "~pubid);
	});
}

static T findEntryByPubid(T=Entry)(in string name, in Entry.Space space, in long id) {
	return _byPubid_impl!T((out string query, out Db.SqlArg[] arguments, bool idOnly)
	{
		query = renderSql!"bypubid/by-Name-Status-Id"(tuple!("idOnly")(idOnly));
		arguments ~= Db.createSqlArg(name);
		arguments ~= Db.createSqlArg(space);
		arguments ~= Db.createSqlArg(id);
	});
}

static T findEntryByPubid(T=Entry)(in string name, in long id) {
	return _byPubid_impl!T((out string query, out Db.SqlArg[] arguments, bool idOnly)
	{
		query = renderSql!"bypubid/by-Name-Status-Id"(tuple!("idOnly")(idOnly));
		arguments ~= Db.createSqlArg(name);
		arguments ~= Db.createSqlArg(Entry.Space.Default);
		arguments ~= Db.createSqlArg(id);
	});
}

static T findEntryByPubid(T=Entry)(in string name, in string id) {
	return _byPubid_impl!T((out string query, out Db.SqlArg[] arguments, bool idOnly)
	{
		immutable auto destringified = _destringifyId(id);
		
		query = renderSql!"bypubid/by-Name-Status-Id"(tuple!("idOnly")(idOnly));
		arguments ~= Db.createSqlArg(name);
		arguments ~= Db.createSqlArg(destringified.space);
		arguments ~= Db.createSqlArg(destringified.id);
	});
}

static T findEntryByPubid(T=Entry)(in string name, in long id, in ulong replyId) {
	return _byPubid_impl!T((out string query, out Db.SqlArg[] arguments, bool idOnly)
	{
		query = renderSql!"bypubid/by-Name-Status-Id-ReplyId"(tuple!("idOnly")(idOnly));
		arguments ~= Db.createSqlArg(name);
		arguments ~= Db.createSqlArg(Entry.Space.Default);
		arguments ~= Db.createSqlArg(id);
		arguments ~= Db.createSqlArg(replyId);
	});
}


///////////////////////////////////////////////////
//
// Aux functions
//
///////////////////////////////////////////////////

/**
	Universal implementation of all overloads of byPubid() method.
	
	When calling this, one must provide a callback that fills variables «query» and «arguments».
 */
private T _byPubid_impl(T)(void delegate(out string, out Db.SqlArg[], bool) getQueryAndArguments) if (
	is(T == Entry) || isNumeric!T
) {
	// Run the custom query & arguments chooser delegate
	string query = void;
	Db.SqlArg[] arguments = void;
	static if (is(T == Entry)) {
		getQueryAndArguments(query, arguments, false);
	} else {
		getQueryAndArguments(query, arguments, true); // get only id
	}
	
	auto result = Db.execSql(query, arguments);
	static if (is(T == Entry)) {
		return PQntuples(result)!=0 ? Entry.fromRow(result, 0) : null;
	} else {
		return PQntuples(result)!=0 ? Row(result).get!T(0) : -1;
	}
}


private auto _destringifyId(in string id) {
	if (auto m = id.match(ctRegex!`^(|draft|ya)(\d+)`)) {
		Tuple!(Entry.Space,"space", ulong,"id") result;
		switch (m.captures[1].toLower()) {
			default:      result.space = Entry.Space.Default; break;
			case "draft": result.space = Entry.Space.Draft;   break;
		}
		result.id = m.captures[2].to!ulong;
		return result;
	} else
		throw new Exception("Invalid id: "~id);
}


private string _stringifyId(in Entry.Space space, in ulong id) pure {
	final switch (space) {
		case Entry.Space.Default:
			return id.to!string;
		case Entry.Space.Draft:
			return "draft"~id.to!string;
	}
}