module ongra.app;

import curtain.activerecord;
import curtain.router;
import ongra;
import std.conv;
import std.datetime;
import std.file;
import std.regex;
import std.string;
import std.traits;
import std.typecons;
import vibe.http.server;
import vibe.utils.dictionarylist;
import ongra;

class App
{
	static HTTPServerRequest  request;
	static HTTPServerResponse response;
	static Ram                ram;
	static MyRouter           router;
	
	struct RuntimeStruct {
		string title;      /// Text to place into <title></title>
		string titleHtml;  /// Text to place into <h1></h1> on microform (if null, then plain title is used)
		bool ajaxMode;
		bool indexable;    /// If true, the meta tag noindex won't be added
		bool showVc;       /// If true, then the design will be prepared for a visitcard
		bool fullWidth;    /// If true, the page will fill the full width of browser's window
		bool errorMode;    /// If true, a special version of logo («logo on fire») will be shown.
		string[] messages; /// Short messages to be shown to the user now
	}
	static RuntimeStruct rt;
	
	/**
		Contains Route object that matched when analyzing query in MyRouter.handleRequest().
		Useful for highlighting selected link in top menu.
	 */
	static Route activeRoute;
	
	shared static this() {
		App.router = new MyRouter;
	}
	
	/**
		Shorthand for outputting text.
	 */
	static void write(T)(T data) {
		App.response.bodyWriter.write(data.to!string);
	}
	
	static void exit(T=ushort)(in T code=0) if (isNumeric!T) {
		std.c.process.exit(code);
	}
	
	static void msgBox(in string title, in string text) {
		App.rt.title = title;
		render!"msgbox"(tuple!("title", "text")(title, text));
	}
	
	/**
		Shorthand to rendering URLs.
		
		Usage:
			App.url!"Post"(["name":"greatperson", "local_id":"13769"])
	*/
	static string url(string name)(string[string] params=string[string].init) {
		return Config.server.Scheme ~ "://" ~ __traits(getMember, Config.routes, name).render(params);
	}
	
	/**
		Shorthand for rendering the same URL as the current, but with other parameters.
	 */
	static string activeUrl(string[string] params=string[string].init) {
		return Config.server.Scheme ~ "://" ~ activeRoute.render(params);
	}
	
	static string activeUrl(DictionaryList!(string, true, 8) params) {
		string[string] convertedParams;
		foreach (key, value; params)
			convertedParams[key] = value;
		return activeUrl(convertedParams);
	}
	
	/**
		Redirect to given URL. Used the same way as App.url().
	 */
	static void redirect(string name)(string[string] params=string[string].init) {
		App.response.redirect(App.url!name(params));
	}
	
	/**
		Get number of unread replies addressed to current user.
	 */
	static ulong numOfUnreadReplies() {
		auto res = Db.execSql(`SELECT COUNT(entry_id) FROM inbox WHERE subscriber_id=$1 AND unread='true'`,
			App.ram.user_id);
		return Row(res).get!ulong(0);
	}
	
	/**
		Log given Throwable to the error log file.
	 */
	static void log(Throwable e) {
		string text = Clock.currTime().toSimpleString() ~ "\n";
		
		for (Throwable t=e; t !is null; t=t.next)
			text ~= " >> (" ~ t.file ~ ":" ~ t.line.to!string ~ ") " ~ t.msg ~ "\n";
		
		foreach (string infoLine; e.info.toString().splitLines()) {
			text ~= infoLine.match(`ongra\.`) ? " >> " : "    ";
			text ~= infoLine ~ "\n";
		}
		
		text ~= "\n";
		std.stdio.writeln(text);
		append(Config.server.ErrorLog, text);
	}
	
	/**
		Write custom text into the error log file. Time will be prepended automatically.
	 */
	static void log(string text) {
		text = Clock.currTime().toSimpleString() ~ "\n" ~ text;
		text ~= (text[$-1] == '\n') ? "\n" : "\n\n";
		append(Config.server.ErrorLog, text);
	}
}