module ongra.paginator;

import ongra;
import std.conv;

class TwoWayPaginator
{
	immutable ulong page;
	immutable bool hasLeft;
	immutable bool hasRight;
	
	this(in ulong page, in ulong count, in ulong pageSize=Config.display.PageSize) {
		this.page = page;
		hasLeft = (page != 1);
		hasRight = (page * pageSize < count);
	}
	
	string leftLink() const {
		return page == 2
			? App.activeUrl(App.request.params)
			: App.activeUrl(App.request.params) ~ "?page=" ~ (page-1).to!string;
	}
	
	string rightLink() const {
		return App.activeUrl(App.request.params) ~ "?page=" ~ (page+1).to!string;
	}
}