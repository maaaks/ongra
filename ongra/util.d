module ongra.util;

import ongra;
import std.base64;
import std.conv;
import std.datetime;
import std.digest.sha;
import std.string;
import std.traits;
import vibe.crypto.cryptorand;


/**
	Returns "1st" for 1, "2nd" for 2, "3rd" for 3, "4th" for 4, etc.
 */
string englishNumeral(T)(T n) if (isIntegral!T) {
	switch (n % 10) {
		case 1:
			return n.to!string ~ "st";
		case 2:
			return n.to!string ~ "nd";
		case 3:
			return n.to!string ~ "rd";
		default:
			return n.to!string ~ "th";
	}
}


/**
	Generates a random string of given length.
	Used for creating Rams, Uploads.
 */
string generateRandomString(uint Length)() {
	auto rng = new SHA1HashMixerRNG();
	ubyte[Length] rand;
	rng.read(rand);
	return Base64Impl!('-', '_', Base64.NoPadding).encode(rand);
}


/**
	Converts date to a string of format like "Sat, 03 Oct 2015 13:24:46 GMT".
 */
string toGmtString(SysTime rawDate) {
	immutable auto d = rawDate.toUTC();
	return d.dayOfWeek.to!string.capitalize() ~ ", "
		~ d.day.to!string.rightJustify(2, '0') ~ " "
		~ d.month.to!string.capitalize() ~ " "
		~ d.year.to!string ~ " "
		~ d.hour.to!string.rightJustify(2, '0') ~ ":"
		~ d.minute.to!string.rightJustify(2, '0') ~ ":"
		~ d.second.to!string.rightJustify(2, '0') ~ " GMT";
}