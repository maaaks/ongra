module ongra.skinner;

import jax.filters;
import ongra;
import std.typecons;


/**
	Checks whether all values in Values struct are the same type
	as corresponding fields in DataSpec struct declaration,
	and returns a DataSpec instance filled with these values.
	
	This is a helper function for using in the template files.
 */
template loadTplData(DataSpec, Values) if (
	_validateTemplateArguments!(DataSpec, Values) && isTuple!Values
) {
	DataSpec loadTplData(Values values=Values.init) {
		DataSpec data = DataSpec();
		foreach(i, name; Values.fieldNames)
			__traits(getMember, data, name) = __traits(getMember, values, name);
		return data;
	}
}

/**
	For each field in Values, checks that:
		1. a field with such name is defined in DataSpec,
		2. and its type in Values is castable to DataSpec.
		
	This is a function for using in CTFE.
 */
private bool _validateTemplateArguments(DataSpec, Values, uint i=0)() {
	static if (i == cast(uint) Values.length) {
		return true;
	}
	else static if (!__traits(hasMember, DataSpec, Values.fieldNames[i])) {
		pragma(msg, "Unknown parameter \"", Values.fieldNames[i], "\"");
		return false;
	}
	else static if (!is(typeof(__traits(getMember, Values, Values.fieldNames[i]))
	: typeof(__traits(getMember, DataSpec, Values.fieldNames[i])))) {
		pragma(msg, "Error initializing parameter \"", Values.fieldNames[i], "\"",
			" (", typeof(__traits(getMember, Values, Values.fieldNames[i])),
			" → ", typeof(__traits(getMember, DataSpec, Values.fieldNames[i])), ")");
		return false;
	}
	else {
		return _validateTemplateArguments!(DataSpec, Values, i+1);
	}
}

string renderString(string _fileName, T)(T _arguments) {
	string _result;
	
	void write(T)(T str) { _result ~= str; }
	string writable(T)(in T x) { return std.conv.to!string(x); }
	
	mixin(import(_fileName));
	return _result;
}

void render(string fileName, T)(T arguments) if (isTuple!T) {
	App.write(renderString!("skin/"~fileName~".tpl.html.d.mixin", T)(arguments));
}

void render(string fileName)() {
	App.write(renderString!("skin/"~fileName~".tpl.html.d.mixin")(tuple!()));
}

string renderEmail(string fileName, T)(T arguments) if (isTuple!T) {
	return renderString!("skin/emails/"~fileName~".tpl.html.d.mixin", T)(arguments);
}

string renderEmail(string fileName)() {
	return renderString!("skin/emails/"~fileName~".tpl.html.d.mixin")(tuple!());
}

string renderSql(string fileName, T)(T arguments) if (isTuple!T) {
	return renderString!("sql/"~fileName~".tpl.sql.d.mixin", T)(arguments);
}

string renderSql(string fileName)() {
	return renderString!("sql/"~fileName~".tpl.sql.d.mixin")(tuple!());
}