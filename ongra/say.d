module ongra.say;

import kxml.xml;
import ongra;
import std.file;
import std.regex;
import std.typecons;


class Language
{
	/// All loaded instances of Language, indexed by their names.
	private static Language[string] instances;
	
	/// Name of the language: "en", "ru", and so on.
	string code;
	
	/// List of singular and plural forms availiable in the language.
	/// Each form has a regular expression which should detect all numbers to which it is applies.
	Tuple!(string, "name", Regex!char, "regex")[] forms;
	
	/// Translations of messages, indexed by category names and then message ids.
	string[string][string] messages;
	
	
	private this(const string fileName) {
		// Parse the XML file
		XmlNode file = readDocument(readText(fileName));
		
		// Load named plural forms
		foreach (XmlNode plural; file.parseXPath("translation/plurals/plural"))
			forms ~= tuple!("name", "regex")(plural.getAttribute("name"), regex(plural.getCData().strip()));
		
		// Load translations for messages
		foreach (XmlNode category; file.parseXPath("translation/category")) {
			string categoryName = category.getAttribute("name");
			messages[categoryName] = string[string].init;
			foreach (XmlNode tr; category.parseXPath("tr"))
				messages[categoryName][ tr.getAttribute("id") ] = tr.getCData();
		}
		
		// Save this instance using its language code
		string lang = file.parseXPath("translation")[0].getAttribute("lang");
		instances[lang] = this;
	}
	
	static Language load(const string fileName) {
		return new Language(fileName);
	}
	
	static Language current() {
		return instances[ Config.display.DefaultLanguage ];
	}
}


mixin template Phrase(string phrase, string defaultTranslation=null)
{
	import std.algorithm.searching;
	import std.array;
	import std.conv;
	import std.regex;
	import std.string;
	import std.traits;
	
	string say(string messageId)()
	if (messageId == phrase) {
		// Split messageId into category name and the rest of it
		auto slashPos = messageId.indexOf('/');
		string categoryName = messageId[0..slashPos];
		string messageName = messageId[slashPos+1..$];
		
		// Try to get the translation from the current language
		if (auto category = categoryName in Language.current().messages)
			if (auto translation = messageName in *category)
				return *translation;
		
		// If default translation was provided, use it
		if (defaultTranslation)
			return defaultTranslation;
		
		// Else, fallback to the second part of messageId as is
		return messageName;
	}

	string say(string messageId, T)(T num)
	if (messageId == phrase && isIntegral!T) {
		string replacer(Captures!string m) {
			// Convert the number to a string, for future matching against regular expressions
			string numString = num.to!string;
			
			// Split subphrase to get options how it can be written with different plural forms
			string[] options = m[1].split("|");
			
			// Iterate through declared plural forms and find first one which satisfies following conditions:
			//   1. The provided number matches the form's regex.
			//   2. The form's name is mentioned in one of options in the message text.
			foreach (form; Language.current().forms)
				if (numString.matchFirst(form.regex))
					foreach (option; options)
						if (option.canFind(form.name))
							return option.replace(form.name, numString);
			
			// Otherwise, just return the first option of the subphrase without any substitutions
			return options[0];
		}
		
		return say!messageId.replaceAll!replacer(ctRegex!`(?<!\\)\{(.+)?(?<!\\)\}`);
	}
}