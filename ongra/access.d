module ongra.access;

import ongra;

class Access
{
	/////////////////////////////////////////////////
	/////////////////////////////////////////////////
	
	// Access to entries
	
	/////////////////////////////////////////////////
	/////////////////////////////////////////////////
	
	/**
		Returns true if the user (the current user by default) is allowed to see the entry,
		either in blog, in What's New, or on a separate page.
	 */
	static bool canSee(Entry entry, User user=App.ram.user) {
		if (entry.blog.deleted)
			return false;
		
		final switch (entry.status) {
			case Entry.Status.Draft:
				// Draft can be d by its author only
				return user && user.id == entry.author_id;
				
			case Entry.Status.OnModeration:
				// Entry on moderation can be viewed by author or moderator
				return user && (
					(user.id == entry.author_id) || (user.getSubscription(entry.blog) >= Blog.Access.Moderator)
				);
				
			case Entry.Status.Published:
				// Public entries can be viewed by anyone
				return true;
				
			case Entry.Status.Deleted:
				// Deleted entries can't be d by anyone
				return false;
		}
	}
	
	/**
		Returns true if the user is allowed to comment the entry.
	 */
	static bool canComment(Entry entry, User user=App.ram.user) {
		return user && canSee(entry, user);
	}
	
	/**
		Returns true if the user is allowed to edit and delete the entry.
	 */
	static bool canEdit(Entry entry, User user=App.ram.user) {
		final switch (entry.status) {	
			case Entry.Status.Draft:
				return user && user.id == entry.author_id;
				
			case Entry.Status.OnModeration:
			case Entry.Status.Published:
				return user && (
					(user.id == entry.author_id) || (user.getSubscription(entry.blog) >= Blog.Access.Moderator)
				);
				
			case Entry.Status.Deleted:
				return false;
		}
	}
	
	/**
		Returns true if the user is allowed to approve the entry in the blog.
		Only the blog's Moderator or Admin can do it.
	 */
	static bool canApprove(Entry entry, User user=App.ram.user) {
		final switch (entry.status) {
			case Entry.Status.OnModeration:
				return user && user.getSubscription(entry.blog) >= Blog.Access.Moderator;
			
			case Entry.Status.Draft:
			case Entry.Status.Published:
			case Entry.Status.Deleted:
				return false;
		}
	}
	
	
	/////////////////////////////////////////////////
	/////////////////////////////////////////////////
	
	// Access to blogs
	
	/////////////////////////////////////////////////
	/////////////////////////////////////////////////
	
	/**
		Currently, every user can see every blog.
		In future, this method will return false in some cases for private blogs.
	 */
	static bool canSee(Blog blog, User user=App.ram.user) {
		return true;
	}
	
	/**
		Check whether user can write new entries to given blog.
		Note that ability to write doesn't automatically mean ability to be approved.
	 */
	static bool canWrite(Blog blog, User user=App.ram.user) {
		final switch (blog.type) {
			case Blog.Type.Personal:
				return user && user.getSubscription(blog) >= Blog.Access.ReadWrite;
				
			case Blog.Type.Premoderated:
			case Blog.Type.Public:
				return true;
		}
	}
	
	/**
		Returns true if user's post can be published in the blog without premoderation.
	 */
	static bool canWriteImmediately(Blog blog, User user=App.ram.user) {
		final switch (blog.type) {
			case Blog.Type.Personal:
				return user && user.getSubscription(blog) >= Blog.Access.ReadWrite;
				
			case Blog.Type.Premoderated:
				return user && user.getSubscription(blog) >= Blog.Access.Moderator;
				
			case Blog.Type.Public:
				return true;
		}
	}
	
	/**
		Returns true if user is allowed to delete the blog.
	 */
	static bool canDelete(Blog blog, User user=App.ram.user) {
		return user && user.id == blog.owner_id;
	}
	
	/**
		Returns true if user is allowed to change blog's settings.
	 */
	static bool canChangeSettings(Blog blog, User user=App.ram.user) {
		return user && user.id == blog.owner_id;
	}
}