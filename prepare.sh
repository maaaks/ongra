#!/bin/bash

# Compile templates using Jax
jax --line-numbers skin/blog.tpl.html || exit
jax --line-numbers skin/blogslist.tpl.html || exit
jax --line-numbers skin/credits.tpl.html || exit
jax --line-numbers skin/deleteblog.tpl.html || exit
jax --line-numbers skin/editorschoice.tpl.html || exit
jax --line-numbers skin/emails/activation.tpl.html || exit
jax --line-numbers skin/inbox.tpl.html || exit
jax --line-numbers skin/login.tpl.html || exit
jax --line-numbers skin/msgbox.tpl.html || exit
jax --line-numbers skin/newblog.tpl.html || exit
jax --line-numbers skin/parts/allcomments.tpl.html || exit
jax --line-numbers skin/parts/bottom.tpl.html || exit
jax --line-numbers skin/parts/comment.tpl.html || exit
jax --line-numbers skin/parts/microtop.tpl.html || exit
jax --line-numbers skin/parts/post.tpl.html || exit
jax --line-numbers skin/parts/top.tpl.html || exit
jax --line-numbers skin/parts/twowaypaginator.tpl.html || exit
jax --line-numbers skin/post.tpl.html || exit
jax --line-numbers skin/preview.tpl.html || exit
jax --line-numbers skin/whatsnew.tpl.html || exit
jax --line-numbers skin/write.tpl.html || exit
jax --line-numbers skin/zh.tpl.html || exit

# Compile SQL queries using Jax
jax --line-numbers sql/AllPosts.tpl.sql || exit
jax --line-numbers sql/auth/Classic.tpl.sql || exit
jax --line-numbers sql/BlogsByUser.tpl.sql || exit
jax --line-numbers sql/Blog.tpl.sql || exit
jax --line-numbers sql/bypubid/by-Name-Status-Id-ReplyId.tpl.sql || exit
jax --line-numbers sql/bypubid/by-Name-Status-Id.tpl.sql || exit
jax --line-numbers sql/count/CountAllPosts.tpl.sql || exit
jax --line-numbers sql/count/CountEditorsChoice.tpl.sql || exit
jax --line-numbers sql/count/CountInbox.tpl.sql || exit
jax --line-numbers sql/count/CountWhatsNew.tpl.sql || exit
jax --line-numbers sql/EditorsChoice.tpl.sql || exit
jax --line-numbers sql/functions.tpl.sql || exit
jax --line-numbers sql/Inbox.tpl.sql || exit
jax --line-numbers sql/NumOfFollowers.tpl.sql || exit
jax --line-numbers sql/Post.tpl.sql || exit
jax --line-numbers sql/RootParent.tpl.sql || exit
jax --line-numbers sql/WhatsNew.tpl.sql || exit

# Compile styles with LESS
lessc --rootpath=/ --strict-units=on design/less/_style.less > design/ongra.css || exit